﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Dto;
using BussyBus.Utilities;
using Microsoft.AspNetCore.Http;
using BussyBus.Filter;
using LinqKit;

namespace BussyBus.Controllers
{
    public class PassengersController : Controller
    {
        private readonly BussyBusDBContext _context;

        public PassengersController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Passengers
        public async Task<IActionResult> Index(string errorMessage, string message, PassengerFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Name");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listPassengers = new List<Passenger>();
            listPassengers = _context.Passengers.Include(u => u.UserAccount).ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_CODE", "By ascending code");
            dictionaryOrdering.Add("DSC_CODE", "By descending code");
            dictionaryOrdering.Add("ASC_NAME", "By ascending name");
            dictionaryOrdering.Add("DSC_NAME", "By descending name");
            dictionaryOrdering.Add("ASC_SURNAME", "By ascending surname");
            dictionaryOrdering.Add("DSC_SURNAME", "By descending surname");
            dictionaryOrdering.Add("ASC_PHONE", "By ascending phone");
            dictionaryOrdering.Add("DSC_PHONE", "By descending phone");
            dictionaryOrdering.Add("ASC_USERNAME", "By ascending username");
            dictionaryOrdering.Add("DSC_USERNAME", "By descending username");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Passenger>(true);
            if (filters.CodeMax != 0)
                predicado = predicado.And(i => i.Code <= filters.CodeMax);
            if (filters.CodeMin != 0)
                predicado = predicado.And(i => i.Code >= filters.CodeMin);
            if (!string.IsNullOrEmpty(filters.Name))
                predicado = predicado.And(i => i.Name.Contains(filters.Name));
            if (!string.IsNullOrEmpty(filters.Surname))
                predicado = predicado.And(i => i.Surname.Contains(filters.Surname));
            if (!string.IsNullOrEmpty(filters.Phone))
                predicado = predicado.And(i => i.Phone.Contains(filters.Phone));
            if (!string.IsNullOrEmpty(filters.Username))
                predicado = predicado.And(i => i.UserAccount.Username.Contains(filters.Username));

            switch (filters.Ordering)
            {
                case "ASC_CODE":
                    listPassengers = listPassengers.OrderBy(t => t.Code).ToList();
                    break;
                case "DSC_CODE":
                    listPassengers = listPassengers.OrderByDescending(t => t.Code).ToList();
                    break;
                case "ASC_NAME":
                    listPassengers = listPassengers.OrderBy(t => t.Name).ToList();
                    break;
                case "DSC_NAME":
                    listPassengers = listPassengers.OrderByDescending(t => t.Name).ToList();
                    break;
                case "ASC_SURNAME":
                    listPassengers = listPassengers.OrderBy(t => t.Surname).ToList();
                    break;
                case "DSC_SURNAME":
                    listPassengers = listPassengers.OrderByDescending(t => t.Surname).ToList();
                    break;
                case "ASC_PHONE":
                    listPassengers = listPassengers.OrderBy(t => t.Phone).ToList();
                    break;
                case "DSC_PHONE":
                    listPassengers = listPassengers.OrderByDescending(t => t.Phone).ToList();
                    break;
                case "ASC_USERNAME":
                    listPassengers = listPassengers.OrderBy(t => t.UserAccount.Username).ToList();
                    break;
                case "DSC_USERNAME":
                    listPassengers = listPassengers.OrderByDescending(t => t.UserAccount.Username).ToList();
                    break;
                default:
                    listPassengers = listPassengers.OrderBy(t => t.Name).ToList();
                    break;
            }

            ViewBag.listAdmins = listPassengers.Where(predicado);

            return View();
        }

        // GET: Passengers/Details/5
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdminPassenger))]
        public async Task<IActionResult> Details(int? id)
        {
            var userId = HttpContext.Session.GetInt32("userId");
            var passengerDBContext = _context.Passengers.Include(c => c.UserAccount);
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userDBContext = _context.Passengers.Include(c => c.UserAccount);

            var passenger = userDBContext.First();
            if (nameRole.Name.Equals("ADMIN"))
            {
                passenger = userDBContext.Where(a => a.ID.Equals(id)).First();
            }
            else
            {
                passenger = userDBContext.Where(u => u.UserAccountId.Equals(userId)).First();

            }

            var passengerAccountDBContext = _context.UserAccounts;
            var passengerAccount = passengerAccountDBContext.Where(u => u.ID.Equals(userId)).First();

            PassengerDto passengerDto = new PassengerDto
            {
                ID = passenger.UserAccountId,
                Username = passenger.UserAccount.Username,
                Password = passenger.UserAccount.Password,
                ConfirmPassword = passenger.UserAccount.Password,
                Email = passenger.UserAccount.Email,
                Name = passenger.Name,
                Surname = passenger.Surname,
                Phone = passenger.Phone,
                Active = passenger.UserAccount.Active
            };

            return View(passengerDto);
        }

        // GET: Passengers/Create
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Password");
            return View();
        }

        // POST: Passengers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone, Code")] PassengerDto passengerDto)
        {
            if (ModelState.IsValid)
            {

                //Comprobar password iguales
                var role = _context.Roles.Where(r => r.Name.Equals("PASSENGER")).First();
                if (passengerDto.Password != passengerDto.ConfirmPassword)
                {
                    return RedirectToAction("Create", "PassengerDtos");
                }

                //Comprobar que no existe el nombre de usuario
                //Comprobar que el email no existe ya en una passenger
                var mismoUsuario = _context.Drivers.Include(u => u.UserAccount)
                .Where(u => u.UserAccount.Username.Equals(passengerDto.Username) || u.UserAccount.Email.Equals(passengerDto.Email)).ToList();
                var mismoUs = _context.Passengers.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username == passengerDto.Username || u.UserAccount.Email == passengerDto.Email).ToList();

                if (mismoUsuario.Count != 0 || mismoUs.Count != 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "account username or email already created" });
                }

                //comprobamos que el codigo de passenger es unico
                var passengerCodeRepeat = _context.Passengers.Include(u => u.UserAccount).Where(a => a.Code == passengerDto.Code).ToList();
                if (passengerCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "Passenger code already assingned to another passenger." });
                }

                //Crear un UserAccount
                UserAccount userAccount = new UserAccount();
                userAccount.Username = passengerDto.Username;
                userAccount.Password = passengerDto.Password;
                userAccount.RoleId = role.ID;
                userAccount.Active = true;
                userAccount.Email = passengerDto.Email;

                userAccount.Password = Utility.Encrypt(userAccount.Password);

                _context.UserAccounts.Add(userAccount);
                await _context.SaveChangesAsync();


                //Crear un passenger
                Passenger passenger = new Passenger();
                passenger.UserAccountId = userAccount.ID;
                passenger.Code = passenger.Code;
                passenger.Name = passengerDto.Name;
                passenger.Surname = passengerDto.Surname;
                passenger.Phone = passengerDto.Phone;

                _context.Passengers.Add(passenger);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Login");
            }
            return View(passengerDto);
        }

        // GET: Passengers/Edit/5
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdminPassenger))]
        public async Task<IActionResult> Edit(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var passengerDBContext = _context.Passengers.Include(c => c.UserAccount);
            var passenger = passengerDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            var passengerAccountDBContext = _context.UserAccounts;
            var passengerAccount = passengerAccountDBContext.Where(u => u.ID.Equals(userId)).First();

            PassengerDto passengerDto = new PassengerDto
            {
                ID = passenger.UserAccountId,
                Username = passenger.UserAccount.Username,
                Password = passenger.UserAccount.Password,
                ConfirmPassword = passenger.UserAccount.Password,
                Email = passenger.UserAccount.Email,
                Name = passenger.Name,
                Surname = passenger.Surname,
                Phone = passenger.Phone,
                Active = passenger.UserAccount.Active
            };

            return View(passengerDto);
        }

        // POST: Passengers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdminPassenger))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone")] PassengerDto passengerDto)
        {

            if (ModelState.IsValid)
            {
                if (id != passengerDto.ID)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    var userId = HttpContext.Session.GetInt32("userId");
                    var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                    
                    
                    if (b.Password.Equals(Utility.Encrypt(passengerDto.Password)) && passengerDto.Password.Equals(passengerDto.ConfirmPassword))
                    {
                        //modificamos el useraccount
                        var role = _context.Roles.Where(r => r.Name.Equals("PASSENGER")).First();

                        b.ID = (int)userId;
                        b.Username = passengerDto.Username;
                        b.Email = passengerDto.Email;
                        b.Active = passengerDto.Active;
                        b.RoleId = role.ID;

                        _context.UserAccounts.Update(b);
                        await _context.SaveChangesAsync();

                        //modificamos el passenger
                        var p = _context.Passengers.Where(c => c.UserAccountId.Equals(passengerDto.ID)).First();
                        p.Name = passengerDto.Name;
                        p.Surname = passengerDto.Surname;
                        p.Phone = passengerDto.Phone;
                        p.UserAccountId = (int)userId;

                        _context.Passengers.Update(p);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home", new { errorMessage = "Incorrect password" });
                    }
                }
            }
            return View(passengerDto);
        }

        private bool PassengerExists(int id)
        {
            return _context.Passengers.Any(e => e.ID == id);
        }
        public async Task<IActionResult> ChangePassword(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var passengerDBContext = _context.Passengers.Include(c => c.UserAccount);
            var driver = passengerDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            PasswordProfileDto passwordProfileDto = new PasswordProfileDto();
            passwordProfileDto.ID = driver.ID;
            passwordProfileDto.OldPassword = driver.UserAccount.Password;


            return View(passwordProfileDto);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(int id, [Bind("ID,Email,OldPassword,Password,ConfirmPassword")] PasswordProfileDto passwordProfileDto)
        {
            if (id != passwordProfileDto.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = HttpContext.Session.GetInt32("userId");
                    var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                    //Comprobamos que Oldpassword sea correcta
                    if (!Utility.Encrypt(passwordProfileDto.OldPassword).Equals(b.Password))
                    {
                        return RedirectToAction("ChangePassword", "Passengers", new { errorMessage = "Incorrect password" });
                    }
                    //Comprobamos que password y ConfirmPassword sean iguales
                    if (passwordProfileDto.Password.Equals(passwordProfileDto.ConfirmPassword))
                    {
                        //modificamos el useraccount

                        b.ID = (int)userId;
                        b.Password = Utility.Encrypt(passwordProfileDto.Password);

                        _context.UserAccounts.Update(b);
                        await _context.SaveChangesAsync();

                    }
                    else
                    {
                        return RedirectToAction("ChangePassword", "Passengers", new { errorMessage = "New password and confirm password not the same" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", "Passengers", new { errorMessage = "Password changed" });
            }
            return View(passwordProfileDto);
        }
    }
}
