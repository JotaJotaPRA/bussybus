﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdPasDri))]
    public class BusStopsController : Controller
    {
        private readonly BussyBusDBContext _context;

        public BusStopsController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: BusStops
        public async Task<IActionResult> Index(string errorMessage, string message, BusStopFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listBusStops = new List<BusStop>();

            listBusStops = _context.BusStops.ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_STOPNUMBER", "By asceding bus stop number");
            dictionaryOrdering.Add("DSC_STOPNUMBER", "By desceding bus stop number");
            dictionaryOrdering.Add("ASC_STOPDIRECTION", "By asceding bus stop direction");
            dictionaryOrdering.Add("DSC_STOPDIRECTION", "By desceding bus stop direction");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<BusStop>(true);
            if (filters.NumberMax != 0)
                predicado = predicado.And(i => i.Number <= filters.NumberMax);
            if (filters.NumberMin != 0)
                predicado = predicado.And(i => i.Number >= filters.NumberMin);
            if (!string.IsNullOrEmpty(filters.Direction))
                predicado = predicado.And(i => i.Direction.Contains(filters.Direction));

            switch (filters.Ordering)
            {
                case "ASC_STOPNUMBER":
                    listBusStops = listBusStops.OrderBy(t => t.Number).ToList();
                    break;
                case "DSC_STOPNUMBER":
                    listBusStops = listBusStops.OrderByDescending(t => t.Number).ToList();
                    break;
                case "ASC_STOPDIRECTION":
                    listBusStops = listBusStops.OrderBy(t => t.Direction).ToList();
                    break;
                case "DSC_STOPDIRECTION":
                    listBusStops = listBusStops.OrderByDescending(t => t.Direction).ToList();
                    break;

                default:
                    listBusStops = listBusStops.OrderBy(t => t.Number).ToList();
                    break;
            }

            ViewBag.listBusStops = listBusStops.Where(predicado);

            return View();
        }

        // GET: BusStops/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busStop = await _context.BusStops
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busStop == null)
            {
                return NotFound();
            }

            return View(busStop);
        }
        [ServiceFilter(typeof(SecurityAdmin))]
        // GET: BusStops/Create
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            return View();
        }

        // POST: BusStops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Create([Bind("ID,Number,Direction")] BusStop busStop)
        {
            if (ModelState.IsValid)
            {
                //comprobamos que el codigo de busHasStop es unico
                var busStopNumberRepeat = _context.BusStops.Where(b => b.Number == busStop.Number).ToList();
                if (busStopNumberRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "BusStop code already assingned to another busStop." });
                }
                _context.Add(busStop);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(busStop);
        }

        // GET: BusStops/Edit/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busStop = await _context.BusStops.FindAsync(id);
            if (busStop == null)
            {
                return NotFound();
            }
            return View(busStop);
        }

        // POST: BusStops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Number,Direction")] BusStop busStop)
        {
            if (id != busStop.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(busStop);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BusStopExists(busStop.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(busStop);
        }

        // GET: BusStops/Delete/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busStop = await _context.BusStops
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busStop == null)
            {
                return NotFound();
            }

            return View(busStop);
        }

        // POST: BusStops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var busStop = await _context.BusStops.FindAsync(id);
            var busHasStop = _context.BusHasStops.Include(t => t.BusStop).Where(b => b.BusStop.ID == id);
            if (busHasStop != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Bus stop has busHasStop associated." });
            }
            var journey = _context.Journeys.Include(j => j.StartBusStop).Include(j => j.EndBusStop).Where(j => j.StartBusStop.ID == id || j.EndBusStop.ID == id);
            if (busHasStop != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Bus stop has journey associated." });
            }
            _context.BusStops.Remove(busStop);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BusStopExists(int id)
        {
            return _context.BusStops.Any(e => e.ID == id);
        }
    }
}
