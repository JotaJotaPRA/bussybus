﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using Microsoft.AspNetCore.Http;
using NHibernate.Mapping;
using Grpc.Core;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using SpreadsheetLight;
using ClosedXML.Excel;
using System.IO;




namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdminPassenger))]
    public class TicketsController : Controller
    {
        private readonly BussyBusDBContext _context;

        public TicketsController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Tickets
        public async Task<IActionResult> Index(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");
            var userId = HttpContext.Session.GetInt32("userId");
            var bussyBusDBContext = _context.Tickets.Include(t => t.Passenger);
            if (nameRole.Name.Equals("PASSENGER"))
            {
                var passenger = _context.Passengers.Include(p => p.UserAccount).Where(a => a.UserAccountId.Equals(userId)).First();
                return View(await _context.Tickets.Include(t => t.Passenger).Where(t => t.Passenger.Equals(passenger)).ToListAsync());
            }
            else
            {
                bussyBusDBContext = _context.Tickets.Include(t => t.Passenger);
            }
            return View(await bussyBusDBContext.ToListAsync());
        }

        // GET: Tickets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticket = await _context.Tickets
                .Include(t => t.Passenger)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (ticket == null)
            {
                return NotFound();
            }

            return View(ticket);
        }

        // GET: Tickets/Create
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityPassenger))]
        public IActionResult Create(int id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["EndBusStop"] = new SelectList(_context.BusStops, "Direction", "Direction");
            ViewData["StartBusStop"] = new SelectList(_context.BusStops, "Direction", "Direction");
            ViewData["CreditCard"] = new SelectList(_context.Buses, "CreditCardNumber", "CreditCardNumber");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityPassenger))]
        public async Task<IActionResult> Create([Bind("ID,Date,EndBusStop,StartBusStop,PassengerID")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                ticket.ListTicketHasJourneys = new List<TicketHasJourney>();
                var userRole = HttpContext.Session.GetString("userRole");
                var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
                var userId = HttpContext.Session.GetInt32("userId");
                var user = _context.Passengers.Include(c => c.UserAccount)
                    .Where(c => c.UserAccountId == userId).FirstOrDefault();

                ticket.Passenger = user;
                ticket.PassengerID = user.ID;
                ticket.Date = DateTime.Now;
                
                //Se crean los TicketHasJourneys necesarios, cada uno representa un tramo
                //Buscamos un Journey con ambas paradas
                bool exito = false;
                List<Journey> tramos =_context.Journeys.Include(j => j.StartBusStop).Include(j => j.EndBusStop).Include(j => j.Bus).Include(j => j.ListTicketHasJourneys)
                    .Where(j => j.StartBusStop.Direction.Equals(ticket.StartBusStop) && j.JourneyDate > DateTime.Now
                    && j.EndBusStop.Direction.Equals(ticket.EndBusStop)).ToList();
                // && j.Bus.Capacity <= j.ListTicketHasJourneys.Count
                //Les asignamos la lista de tickets que tienen para ese journey
                for(int i=0; i<tramos.Count; i++)
                {
                    tramos[i].ListTicketHasJourneys = _context.TicketHasJourneys.Include(j => j.Journey).ToList();
                }
                //Nueva lista solo aforo disponible
                List<Journey> tramosAforo = new List<Journey>();
                for(int i=0; i<tramos.Count; i++)
                {
                    if (tramos[i].ListTicketHasJourneys.Count < tramos[i].Bus.Capacity)
                    {
                        tramosAforo.Add(tramos[i]);
                    }
                }
                //No se selecciona entre tramos (añadir sistema checkout)
                if (tramos.Count > 0)
                {
                    TicketHasJourney tramo = new TicketHasJourney();
                    
                    tramo.Ticket = ticket;
                    tramo.TicketID = tramo.Ticket.ID;
                    tramo.Journey = tramos.First();
                    tramo.JourneyID = tramo.Journey.ID;
                    tramo.Journey.ListTicketHasJourneys = _context.TicketHasJourney.Include(j => j.Journey).Where(j => j.JourneyID == tramo.JourneyID).ToList();
                    ticket.Total = tramo.Journey.Price;
                    
                    _context.Add(tramo);  
                } else if (tramos.Count == 0) //Comprobamos si se puede hacer en 2 tramos
                {
                    List<Journey> tramos0 = _context.Journeys.Include(j => j.StartBusStop).Include(j => j.EndBusStop).Where(j => j.StartBusStop.Direction.Equals(ticket.StartBusStop)
                    || j.EndBusStop.Direction.Equals(ticket.EndBusStop)).ToList();
                    if (tramos0.Count > 1)
                    {
                        for (int i = 0; i < tramos0.Count; i++)
                        {
                            if (tramos0[i].StartBusStop.Direction.Equals(ticket.StartBusStop))
                            {
                                for (int j = i + 1; j < tramos0.Count; j++)
                                {
                                    if (tramos0[i].EndBusStop.Equals(tramos0[j].StartBusStop) || tramos0[i].StartBusStop.Equals(tramos0[j].EndBusStop) && tramos0[i].JourneyDate < tramos[j].JourneyDate && tramos0[i].JourneyDate > DateTime.Now && tramos0[j].JourneyDate > DateTime.Now)
                                    {
                                        TicketHasJourney tramo1 = new TicketHasJourney();
                                        TicketHasJourney tramo2 = new TicketHasJourney();
                                        tramo1.Ticket = ticket;
                                        tramo2.Ticket = ticket;
                                        tramo1.Journey = tramos0[i];
                                        tramo2.Journey = tramos0[j];
                                        ticket.Total = tramo1.Journey.Price + tramo2.Journey.Price;

                                        /*for(int k=0; k<tramo1.Journey.ListTicketHasJourneys.Count; k++)
                                        {
                                            ticket.ListTicketHasJourneys.Add(tramo1.Journey.ListTicketHasJourneys[k]);
                                        }
                                        for (int k = 0; k < tramo2.Journey.ListTicketHasJourneys.Count; k++)
                                        {
                                            ticket.ListTicketHasJourneys.Add(tramo2.Journey.ListTicketHasJourneys[k]);
                                        }*/
                                        _context.Add(tramo1);
                                        _context.Add(tramo2);
                                        exito = true;
                                    }
                                    if (exito)
                                    {
                                        i = tramos0.Count;
                                        j = tramos0.Count;
                                    }
                                }
                            }
                        }
                    } else // Sin exito, no se puede hacer el viaje
                    {
                        return RedirectToAction(nameof(Create), new { errorMessage = "journey not possible" });
                    }

                } else //Sin exito, no se puede hacer el viaje
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "journey not possible" });
                }
                _context.Add(ticket);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PassengerID"] = new SelectList(_context.Passengers, "ID", "Name", ticket.PassengerID);
            return View(ticket);
        }

        

        private bool TicketExists(int id)
        {
            return _context.Tickets.Any(e => e.ID == id);
        }
        public IActionResult CrearExcel()
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Ventas");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Fecha";
                worksheet.Cell(currentRow, 2).Value = "Start";
                worksheet.Cell(currentRow, 3).Value = "End";
                worksheet.Cell(currentRow, 4).Value = "Total";

                List<Ticket> listTickets = _context.Tickets.Where(t => t.Date >= DateTime.Now.AddMonths(-1)).ToList(); //Tickets del último mes
                foreach(var ticket in listTickets)
                {
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = ticket.Date.ToString();
                    worksheet.Cell(currentRow, 2).Value = ticket.StartBusStop;
                    worksheet.Cell(currentRow, 3).Value = ticket.EndBusStop;
                    worksheet.Cell(currentRow, 4).Value = ticket.Total;
                }
                using(var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "Ventas.xlsx"
                        );
                }


            }
            
        }

    }
}
