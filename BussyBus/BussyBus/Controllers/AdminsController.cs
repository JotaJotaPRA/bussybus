﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Dto;
using BussyBus.Utilities;
using Microsoft.AspNetCore.Http;
using BussyBus.Filter;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdmin))]
    public class AdminsController : Controller
    {
        private readonly BussyBusDBContext _context;

        public AdminsController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Admins
        public async Task<IActionResult> Index(string errorMessage, string message, AdminFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Name");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");
            
            var listAdmins = new List<Admin>();
            listAdmins = _context.Admins.Include(u => u.UserAccount).ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_CODE", "By ascending code");
            dictionaryOrdering.Add("DSC_CODE", "By descending code");
            dictionaryOrdering.Add("ASC_NAME", "By ascending name");
            dictionaryOrdering.Add("DSC_NAME", "By descending name");
            dictionaryOrdering.Add("ASC_SURNAME", "By ascending surname");
            dictionaryOrdering.Add("DSC_SURNAME", "By descending surname");
            dictionaryOrdering.Add("ASC_PHONE", "By ascending phone");
            dictionaryOrdering.Add("DSC_PHONE", "By descending phone");
            dictionaryOrdering.Add("ASC_USERNAME", "By ascending username");
            dictionaryOrdering.Add("DSC_USERNAME", "By descending username");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Admin>(true);
            if (filters.CodeMax != 0)
                predicado = predicado.And(i => i.Code <= filters.CodeMax);
            if (filters.CodeMin != 0)
                predicado = predicado.And(i => i.Code >= filters.CodeMin);
            if (!string.IsNullOrEmpty(filters.Name))
                predicado = predicado.And(i => i.Name.Contains(filters.Name));
            if (!string.IsNullOrEmpty(filters.Surname))
                predicado = predicado.And(i => i.Surname.Contains(filters.Surname));
            if (!string.IsNullOrEmpty(filters.Phone))
                predicado = predicado.And(i => i.Phone.Contains(filters.Phone));
            if (!string.IsNullOrEmpty(filters.Username))
                predicado = predicado.And(i => i.UserAccount.Username.Contains(filters.Username));

            switch (filters.Ordering)
            {
                case "ASC_CODE":
                    listAdmins = listAdmins.OrderBy(t => t.Code).ToList();
                    break;
                case "DSC_CODE":
                    listAdmins = listAdmins.OrderByDescending(t => t.Code).ToList();
                    break;
                case "ASC_NAME":
                    listAdmins = listAdmins.OrderBy(t => t.Name).ToList();
                    break;
                case "DSC_NAME":
                    listAdmins = listAdmins.OrderByDescending(t => t.Name).ToList();
                    break;
                case "ASC_SURNAME":
                    listAdmins = listAdmins.OrderBy(t => t.Surname).ToList();
                    break;
                case "DSC_SURNAME":
                    listAdmins = listAdmins.OrderByDescending(t => t.Surname).ToList();
                    break;
                case "ASC_PHONE":
                    listAdmins = listAdmins.OrderBy(t => t.Phone).ToList();
                    break;
                case "DSC_PHONE":
                    listAdmins = listAdmins.OrderByDescending(t => t.Phone).ToList();
                    break;
                case "ASC_USERNAME":
                    listAdmins = listAdmins.OrderBy(t => t.UserAccount.Username).ToList();
                    break;
                case "DSC_USERNAME":
                    listAdmins = listAdmins.OrderByDescending(t => t.UserAccount.Username).ToList();
                    break;
                default:
                    listAdmins = listAdmins.OrderBy(t => t.Name).ToList();
                    break;
            }

            ViewBag.listAdmins = listAdmins.Where(predicado);

            return View();
        }

        // GET: Admins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var userId = HttpContext.Session.GetInt32("userId");
            var adminDBContext = _context.Admins.Include(c => c.UserAccount);
            var admin = adminDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            var adminAccountDBContext = _context.UserAccounts;
            var adminAccount = adminAccountDBContext.Where(u => u.ID.Equals(userId)).First();
            //var customer = _context.Customers.Find(userId);

            AdminDto adminDto = new AdminDto
            {
                ID = admin.UserAccountId,
                Username = admin.UserAccount.Username,
                Password = admin.UserAccount.Password,
                ConfirmPassword = admin.UserAccount.Password,
                Email = admin.UserAccount.Email,
                Name = admin.Name,
                Surname = admin.Surname,
                Phone = admin.Phone,
                Active = admin.UserAccount.Active
            };

            return View(adminDto);
        }

        // GET: Admins/Create
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Password");
            return View();
        }

        // POST: Admins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone,Code")] AdminDto adminDto)
        {
            if (ModelState.IsValid)
            {


                if (adminDto.Password != adminDto.ConfirmPassword)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "error" });
                }

                var mismoUsuario = _context.Admins.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username.Equals(adminDto.Username) || u.UserAccount.Email.Equals(adminDto.Email)).ToList();
                var mismoUs = _context.Passengers.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username == adminDto.Username || u.UserAccount.Email == adminDto.Email).ToList();
                var mismoUs2 = _context.Drivers.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username == adminDto.Username || u.UserAccount.Email == adminDto.Email).ToList();

                if (mismoUsuario.Count != 0 || mismoUs.Count != 0 || mismoUs2.Count != 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "account username already created" });
                }
                var role = _context.Roles.Where(r => r.Name.Equals("ADMIN")).First();

                //comprobamos que el codigo de admin es unico
                var adminCodeRepeat = _context.Admins.Include(u => u.UserAccount).Where(a => a.Code == adminDto.Code).ToList();
                if(adminCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "Admin code already assingned to another admin." });
                }

                //create userAcount
                UserAccount userAccount = new UserAccount();
                userAccount.Username = adminDto.Username;
                userAccount.Password = adminDto.Password;
                userAccount.RoleId = role.ID;
                userAccount.Email = adminDto.Email;
                userAccount.Active = true;

                userAccount.Password = Utility.Encrypt(userAccount.Password);

                _context.UserAccounts.Add(userAccount);
                await _context.SaveChangesAsync();

                //create admin
                Admin admin = new Admin();
                admin.UserAccountId = userAccount.ID; //esto hace referencia al user account que hemos creado
                admin.Name = adminDto.Name;
                admin.Surname = adminDto.Surname;
                admin.Phone = adminDto.Phone;
                admin.Code = adminDto.Code;

                _context.Admins.Add(admin);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Admins");

            }
            return View(adminDto);
        }

        // GET: Admins/Edit/5
        public async Task<IActionResult> Edit(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var adminDBContext = _context.Admins.Include(c => c.UserAccount);
            var admin = adminDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            var adminAccountDBContext = _context.UserAccounts;
            var adminAccount = adminAccountDBContext.Where(u => u.ID.Equals(userId)).First();
            //var customer = _context.Customers.Find(userId);

            AdminDto adminDto = new AdminDto
            {
                ID = admin.UserAccountId,
                Username = admin.UserAccount.Username,
                Password = admin.UserAccount.Password,
                ConfirmPassword = admin.UserAccount.Password,
                Email = admin.UserAccount.Email,
                Name = admin.Name,
                Surname = admin.Surname,
                Phone = admin.Phone,
                Active = admin.UserAccount.Active
            };

            return View(adminDto);
        }

        // POST: Admins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone")] AdminDto adminDto)
        {
            if (id != adminDto.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = HttpContext.Session.GetInt32("userId");
                    var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                    if (b.Password.Equals(Utility.Encrypt(adminDto.Password)) && adminDto.Password.Equals(adminDto.ConfirmPassword))
                    {
                        //modificamos el useraccount
                        var role = _context.Roles.Where(r => r.Name.Equals("ADMIN")).First();

                        b.ID = (int)userId;
                        b.Username = adminDto.Username;
                        b.Email = adminDto.Email;
                        b.Active = adminDto.Active;
                        b.RoleId = role.ID;

                        _context.UserAccounts.Update(b);
                        await _context.SaveChangesAsync();

                        //modificamos el admin
                        var a = _context.Admins.Where(c => c.UserAccountId.Equals(adminDto.ID)).First();
                        a.Name = adminDto.Name;
                        a.Surname = adminDto.Surname;
                        a.Phone = adminDto.Phone;
                        a.UserAccountId = (int)userId;

                        _context.Admins.Update(a);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home", new { errorMessage = "incorrect password" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Index", "Home");
            }
            return View(adminDto);
        }

        private bool AdminExists(int id)
        {
            return _context.Admins.Any(e => e.ID == id);
        }

        public async Task<IActionResult> ChangePassword(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var adminDBContext = _context.Admins.Include(c => c.UserAccount);
            var admin = adminDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            PasswordProfileDto passwordProfileDto = new PasswordProfileDto();
            passwordProfileDto.ID = admin.ID;
            passwordProfileDto.OldPassword = admin.UserAccount.Password;


            return View(passwordProfileDto);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(int id, [Bind("ID,Email,OldPassword,Password,ConfirmPassword")] PasswordProfileDto passwordProfileDto)
        {
            if (id != passwordProfileDto.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = HttpContext.Session.GetInt32("userId");
                    var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                    //Comprobamos que Oldpassword sea correcta
                    if (!Utility.Encrypt(passwordProfileDto.OldPassword).Equals(b.Password)){
                        return RedirectToAction("ChangePassword", "Admins", new { errorMessage = "Incorrect password" });
                    }
                    //Comprobamos que password y ConfirmPassword sean iguales
                    if (passwordProfileDto.Password.Equals(passwordProfileDto.ConfirmPassword))
                    {
                        //modificamos el useraccount

                        b.ID = (int)userId;
                        b.Password = Utility.Encrypt(passwordProfileDto.Password);

                        _context.UserAccounts.Update(b);
                        await _context.SaveChangesAsync();

                    }
                    else
                    {
                        return RedirectToAction("ChangePassword", "Admins", new { errorMessage = "New password and confirm password not the same" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", "Admins", new { errorMessage = "Password changed" });
            }
            return View(passwordProfileDto);
        }
    }
}
