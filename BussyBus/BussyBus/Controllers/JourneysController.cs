﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdPasDri))]
    public class JourneysController : Controller
    {
        private readonly BussyBusDBContext _context;

        public JourneysController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Journeys
        public async Task<IActionResult> Index(string errorMessage, string message, JourneyFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Name");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listJourneys = new List<Journey>();
            if (nameRole.Name.Equals("PASSENGER"))
            {
                listJourneys = _context.Journeys.Include(j => j.EndBusStop).Include(j => j.StartBusStop).Include(b => b.Bus)
                    .Where(i => i.JourneyDate.CompareTo(DateTime.Now)>0).ToList();
            } else if (nameRole.Name.Equals("DRIVER"))
            {

            }
            
            listJourneys = _context.Journeys.Include(j => j.EndBusStop).Include(j => j.StartBusStop).Include(b => b.Bus).ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_PRICE", "By ascending price");
            dictionaryOrdering.Add("DSC_PRICE", "By descending price");
            dictionaryOrdering.Add("ASC_CODESTART", "By ascending code start");
            dictionaryOrdering.Add("DSC_CODESTART", "By descending code start");
            dictionaryOrdering.Add("ASC_CODEEND", "By ascending code end");
            dictionaryOrdering.Add("DSC_CODEEND", "By descending code end");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Journey>(true);
            if (filters.PriceMax != 0)
                predicado = predicado.And(i => i.Price <= filters.PriceMax);
            if (filters.PriceMin != 0)
                predicado = predicado.And(i => i.Price >= filters.PriceMin);
            if (filters.CodeStart != 0)
                predicado = predicado.And(i => i.StartBusStop.Number == filters.CodeStart);
            if (filters.CodeEnd != 0)
                predicado = predicado.And(i => i.EndBusStop.Number == filters.CodeStart);

            switch (filters.Ordering)
            {
                case "ASC_PRICE":
                    listJourneys = listJourneys.OrderBy(t => t.Price).ToList();
                    break;
                case "DSC_PRICE":
                    listJourneys = listJourneys.OrderByDescending(t => t.Price).ToList();
                    break;
                case "ASC_CODESTART":
                    listJourneys = listJourneys.OrderBy(t => t.StartBusStop.Number).ToList();
                    break;
                case "DSC_CODESTART":
                    listJourneys = listJourneys.OrderByDescending(t => t.StartBusStop.Number).ToList();
                    break;
                case "ASC_CODEEND":
                    listJourneys = listJourneys.OrderBy(t => t.EndBusStop.Number).ToList();
                    break;
                case "DSC_CODEEND":
                    listJourneys = listJourneys.OrderByDescending(t => t.EndBusStop.Number).ToList();
                    break;
                
            }

            ViewBag.listJourneys = listJourneys.Where(predicado);
            return View();
        }

        // GET: Journeys/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journey = await _context.Journeys
                .Include(j => j.EndBusStop)
                .Include(j => j.StartBusStop)
                .Include(j => j.Bus)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (journey == null)
            {
                return NotFound();
            }

            return View(journey);
        }

        // GET: Journeys/Create
        [ServiceFilter(typeof(SecurityAdmin))]
        public IActionResult Create()
        {
            ViewData["EndBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction");
            ViewData["StartBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction");
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            return View();
        }

        // POST: Journeys/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Create([Bind("ID,Price,JourneyDate,StartBusStopID,EndBusStopID, BusID")] Journey journey)
        {
            if (ModelState.IsValid)
            {
                //Comprobamos que no ocurre en el pasado
                if (journey.JourneyDate.CompareTo(DateTime.Now) <= 0)
                {
                    return RedirectToAction("Index", "Journeys", new { errorMessage = "can't create an Journey on the past" });
                }
                //comprobamos que stop tienen bus
                List<BusHasStop> listBusHasStartStop = _context.BusHasStops.Where(d => d.StopID == journey.StartBusStopID).Where(d => d.BusID==journey.BusID).ToList();
                if (listBusHasStartStop.Count == 0)
                {
                    return RedirectToAction("Index", "Journeys", new { errorMessage = "Start bus stop doesn´t have a bus assingned." });
                }
                List<BusHasStop> listBusHasEndStop = _context.BusHasStops.Where(d => d.StopID == journey.EndBusStopID).Where(d => d.BusID == journey.BusID).ToList();
                if (listBusHasEndStop.Count == 0)
                {
                    return RedirectToAction("Index", "Journeys", new { errorMessage = "End bus stop doesn´t have a bus assingned." });
                }
                //Comprobamos que el bus del viaje pase por esas paradas
                if(journey.BusID != listBusHasStartStop.First().BusID || journey.BusID != listBusHasEndStop.First().BusID)
                {
                    return RedirectToAction("Index", "Journeys", new { errorMessage = "Bus selected doesn´t have those stops." });
                }

                if (journey.StartBusStopID == journey.EndBusStopID)
                {
                    return RedirectToAction("Index", "Journeys", new { errorMessage = "Journey cannot start and end in the same stop." });
                }

                _context.Add(journey);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EndBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.EndBusStopID);
            ViewData["StartBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.StartBusStopID);
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            return View(journey);
        }

        // GET: Journeys/Edit/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journey = await _context.Journeys.FindAsync(id);
            if (journey == null)
            {
                return NotFound();
            }
            ViewData["EndBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.EndBusStopID);
            ViewData["StartBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.StartBusStopID);
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            return View(journey);
        }

        // POST: Journeys/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Price,JourneyDate,StartBusStopID,EndBusStopID")] Journey journey)
        {
            if (id != journey.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(journey);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!JourneyExists(journey.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EndBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.EndBusStopID);
            ViewData["StartBusStopID"] = new SelectList(_context.BusStops, "ID", "Direction", journey.StartBusStopID);
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            return View(journey);
        }

        // GET: Journeys/Delete/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journey = await _context.Journeys
                .Include(j => j.EndBusStop)
                .Include(j => j.StartBusStop)
                .Include(j => j.Bus)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (journey == null)
            {
                return NotFound();
            }

            return View(journey);
        }

        // POST: Journeys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var journey = await _context.Journeys.FindAsync(id);
            var ticketHasJourney = _context.TicketHasJourneys.Include(t => t.Journey).Where(r => r.JourneyID == id);
            if (ticketHasJourney != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Journey has ticketHasJourney associated." });
            }
            _context.Journeys.Remove(journey);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool JourneyExists(int id)
        {
            return _context.Journeys.Any(e => e.ID == id);
        }
    }
}
