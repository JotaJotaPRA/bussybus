﻿using BussyBus.Data;
using BussyBus.Dto;
using BussyBus.Models;
using BussyBus.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Controllers
{
    public class LoginController : Controller
    {
        private readonly BussyBusDBContext _context;

        public LoginController(BussyBusDBContext context)
        {
            _context = context;
        }

        public IActionResult Index(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            return View();
        }

        public IActionResult Login(string username, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return RedirectToAction(nameof(Index), new { errorMessage = "password is required" });
            }
            //encriptamos la contraseña
            password = Utility.Encrypt(password);
            //var samePassword = _context.UserAccounts.Where(u => u.Username.Equals(username)
            //        && u.Password.Equals(password)).ToList();


            if (string.IsNullOrEmpty(username))
            {
                return RedirectToAction(nameof(Index), new { errorMessage = "username is required" });
            }
            else if (string.IsNullOrEmpty(password))
            {
                return RedirectToAction(nameof(Index), new { errorMessage = "password is required" });
            }
            else
            {
                var user = _context.UserAccounts.Where(u => u.Username.Equals(username)
                    && u.Password.Equals(password)).ToList();
                if (user.Count() == 0)
                {
                    //si es igual a 0 es que no hay ninguna coincidencia por lo tanto muestra error y vuelve a login
                    return RedirectToAction(nameof(Index), new { errorMessage = "The username and password indicated are not valid" });
                }
                else if (!user.First().Active)
                {
                    return RedirectToAction(nameof(Index), new { errorMessage = "The account is inactive." });
                }
                else
                {
                    HttpContext.Session.SetString("user", user.First().ID.ToString());
                    HttpContext.Session.GetString("user");
                    HttpContext.Session.SetString("userName", user.First().Username.ToString());
                    HttpContext.Session.GetString("userName");
                    HttpContext.Session.SetInt32("userId", user.First().ID);
                    HttpContext.Session.GetInt32("userId");
                    HttpContext.Session.SetString("userRole", user.First().RoleId.ToString());
                    HttpContext.Session.GetString("userRole");


                    Utility.Menus = _context.RoleHasMenus.Include(rm => rm.Menu)
                        .Where(rm => rm.RoleID == user.First().RoleId)
                        .Select(rm => rm.Menu)  // con el select selecciono de la lista de RoleMenu, solo los Menus                                                 
                        .ToList();

                    //si encuentra alguna coincidencia va al controlador de home
                    return RedirectToAction("Index", "Home");
                }
            }

        }


        public IActionResult Logout()
        {
            HttpContext.Session.Remove("user");
            return RedirectToAction("Index");
        }

        public IActionResult RememberPassword(string errorMessage)
        {

            ViewBag.errorMessage = errorMessage;
            return View();
        }


        public IActionResult ResetPassword(string email, string token)
        {
            //creamos un dtopassword y lo llamamos para que mande los datos
            PasswordDto passwordDto = new PasswordDto();
            passwordDto.Email = email;
            passwordDto.Token = token;
            return View(passwordDto);
        }

        //POST
        public IActionResult ChangePassword([Bind("ID, Password", "ConfirmPassword", "Email", "Token")] PasswordDto passwordDto)
        {
            //Check if passwords are the same
            if (passwordDto.Password.Equals(passwordDto.ConfirmPassword))
            {
                //Look for UserToken and email
                //Customer customer = _context.Customers.Where(c => c.Email.Equals(passwordDto.Email)).ToList().FirstOrDefault();
                UserAccount userAccount = _context.UserAccounts.Where(c => c.Email.Equals(passwordDto.Email)).ToList().FirstOrDefault();
                if (userAccount == null)
                    return RedirectToAction("Index", new { errorMessage = "User not found" });
                int userAccountId = userAccount.ID;
                //Check if have active token
                UserToken userToken = _context.UserTokens.Where(u => u.UserAccountId == userAccountId).ToList().FirstOrDefault();
                if (userToken == null)
                    return RedirectToAction("Index", new { errorMessage = "Token not found" });
                //Check if token is valid (date)
                DateTime lifeLimit = userToken.GenerationDate.AddMinutes(userToken.Life);
                if (DateTime.Now.CompareTo(lifeLimit) > 0)
                {
                    return RedirectToAction("Index", new { errorMessage = "Token has expired" });
                }
                //Encrypt
                string passwordEncripted = Utility.Encrypt(passwordDto.Password);
                //Save
                UserAccount user = _context.UserAccounts.Where(u => u.ID == userAccount.ID).ToList().First();
                user.Password = passwordEncripted;
                _context.Update(user);
                _context.SaveChanges();
            }
            else
            {
                return RedirectToAction("ChangePassword", new { errorMessage = "Passwords are not the same" });
            }
            return RedirectToAction("Index", new { errorMessage = "Password changed" });
        }



        public IActionResult SendEmail(String EmailOrUsername)
        {
            var userAccountId = GetUserAccountID(EmailOrUsername);
            if (userAccountId == -1) // no existe ninguna cuenta de usuario
            {
                return RedirectToAction("RememberPassword"
                    , new { errorMessage = "Email or username not valid" });
            }
            //limpiamos los token del usuario
            DeleteTokensByUserID(userAccountId);

            //Generar token
            UserToken token = new UserToken();
            token.GenerationDate = DateTime.Now;
            token.Life = Utility.TTL;
            //encriptar
            token.Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            token.UserAccountId = userAccountId;

            _context.UserTokens.Add(token);
            _context.SaveChanges();

            //Enviar el email con el token
            string emailTo = GetEmailByUserID(userAccountId);
            //string reference = "https://localhost:44389/Login/ResetPassword?email=" +
            //string reference = "https://localhost:44319/Login/ResetPassword?email=" +
            string reference = "https://localhost:44388/Login/ResetPassword?email=" +
                emailTo + "&token=" + token.Token;

            if (emailTo.Equals(""))
                return RedirectToAction("RememberPassword", new { errorMessage = "has been ocurred an error" });
            string subject = "Reset password";
            string body = "To reset the password click on the following link:" +
                "<a href='" + reference + "'> Reset Password </a>";

            Utility.SendEmail(emailTo, subject, body);

            return RedirectToAction("Index");

        }

        public void DeleteTokensByUserID(int UserID)
        {
            var tokens = _context.UserTokens.Where(u => u.UserAccountId == UserID).ToList();

            foreach (UserToken token in tokens)
            {
                _context.UserTokens.Remove(token);
            }
            _context.SaveChanges();
        }

        public string GetEmailByUserID(int UserID)
        {
            string email = "";
            var useAccount = _context.UserAccounts
                .Where(c => c.ID == UserID)
                   .FirstOrDefault();
            email = useAccount.Email;
            /*
            var customer = _context.Customers
                .Where(c => c.UserAccountId == UserID)
                   .FirstOrDefault();
            if (customer != null)
            {
                email = customer.Email;
            }
            else
            {
                var seller = _context.Sellers
                .Where(c => c.UserAccountId == UserID)
                   .FirstOrDefault();
                if (seller != null)
                {
                    email = seller.Email;
                }
                else
                {
                    var admin = _context.Admins
                .Where(c => c.UserAccountId == UserID)
                   .FirstOrDefault();
                    if (trainer != null)
                    {
                        email = admin.Email;
                    }
                }
            }
            */
            return email;
        }

        public int GetUserAccountID(string EmailOrUsername)
        {
            int userAccountId = 1;

            var user = _context.UserAccounts
                .Where(u => u.Username.Equals(EmailOrUsername) || u.Email.Equals(EmailOrUsername))
                .FirstOrDefault();

            if (user == null)
            {
                userAccountId = -1;
            }
            else
            {
                userAccountId = user.ID;
            }
            return userAccountId;
        }
    }
}
