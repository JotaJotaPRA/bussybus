﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityPassenger))]
    public class CreditCardsController : Controller
    {
        private readonly BussyBusDBContext _context;

        public CreditCardsController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: CreditCards
        public async Task<IActionResult> Index(string errorMessage, string message, CreditCardFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listCreditCards = new List<CreditCard>();
            
            var userId = HttpContext.Session.GetInt32("userId");
            if (nameRole.Name.Equals("PASSENGER"))
            {
                var passenger = _context.Passengers.Include(p => p.UserAccount).Where(a => a.UserAccountId.Equals(userId)).First();
                listCreditCards = _context.CreditCards.Include(t => t.Passenger).Where(t => t.Passenger.Equals(passenger)).ToList();
            }


            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_NAME", "By asceding name");
            dictionaryOrdering.Add("DSC_NAME", "By desceding name");
            dictionaryOrdering.Add("ASC_CVV", "By asceding CVV");
            dictionaryOrdering.Add("DSC_CVV", "By desceding CVV");
            dictionaryOrdering.Add("ASC_EXPMONTH", "By asceding expiration month");
            dictionaryOrdering.Add("DSC_EXPMONTH", "By desceding expiration month");
            dictionaryOrdering.Add("ASC_EXPYEAR", "By asceding expiration year");
            dictionaryOrdering.Add("DSC_EXPYEAR", "By desceding expiration year");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<CreditCard>(true);
            
            if (!string.IsNullOrEmpty(filters.Name))
                predicado = predicado.And(i => i.Name.Contains(filters.Name));
            if (filters.ExpirationMonth != 0)
                predicado = predicado.And(i => i.ExpirationMonth == filters.ExpirationMonth);
            if (filters.ExpirationYear != 0)
                predicado = predicado.And(i => i.ExpirationYear == filters.ExpirationYear);

            switch (filters.Ordering)
            {
                case "ASC_NAME":
                    listCreditCards = listCreditCards.OrderBy(t => t.Name).ToList();
                    break;
                case "DSC_NAME":
                    listCreditCards = listCreditCards.OrderByDescending(t => t.Name).ToList();
                    break;
                case "ASC_CVV":
                    listCreditCards = listCreditCards.OrderBy(t => t.CVV).ToList();
                    break;
                case "DSC_CVV":
                    listCreditCards = listCreditCards.OrderByDescending(t => t.CVV).ToList();
                    break;
                case "ASC_EXPMONTH":
                    listCreditCards = listCreditCards.OrderBy(t => t.ExpirationMonth).ToList();
                    break;
                case "DSC_EXPMONTH":
                    listCreditCards = listCreditCards.OrderByDescending(t => t.ExpirationMonth).ToList();
                    break;
                case "ASC_EXPYEAR":
                    listCreditCards = listCreditCards.OrderBy(t => t.ExpirationYear).ToList();
                    break;
                case "DSC_EXPYEAR":
                    listCreditCards = listCreditCards.OrderByDescending(t => t.ExpirationYear).ToList();
                    break;

                default:
                    listCreditCards = listCreditCards.OrderBy(t => t.Name).ToList();
                    break;
            }

            ViewBag.listCreditCards = listCreditCards.Where(predicado);

            return View();
        }

        // GET: CreditCards/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _context.CreditCards
                .Include(c => c.Passenger)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (creditCard == null)
            {
                return NotFound();
            }

            return View(creditCard);
        }

        // GET: CreditCards/Create
        public IActionResult Create()
        {
            ViewData["PassengerID"] = new SelectList(_context.Passengers, "ID", "Name");
            return View();
        }

        // POST: CreditCards/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,CreditCardNumber,CVV,ExpirationMonth,ExpirationYear,PassengerID")] CreditCard creditCard)
        {
            if (ModelState.IsValid)
            {
                var userRole = HttpContext.Session.GetString("userRole");
                var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
                var userId = HttpContext.Session.GetInt32("userId");
                var user = _context.Passengers.Include(c => c.UserAccount)
                    .Where(c => c.UserAccountId == userId).FirstOrDefault();

                creditCard.Passenger = user;
                creditCard.PassengerID = user.ID;

                _context.Add(creditCard);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PassengerID"] = new SelectList(_context.Passengers, "ID", "Name", creditCard.PassengerID);
            return View(creditCard);
        }

        // GET: CreditCards/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _context.CreditCards.FindAsync(id);
            if (creditCard == null)
            {
                return NotFound();
            }
            ViewData["PassengerID"] = new SelectList(_context.Passengers, "ID", "Name", creditCard.PassengerID);
            return View(creditCard);
        }

        // POST: CreditCards/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,CreditCardNumber,CVV,expirationMonth,expirationYear,PassengerID")] CreditCard creditCard)
        {
            if (id != creditCard.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(creditCard);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CreditCardExists(creditCard.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PassengerID"] = new SelectList(_context.Passengers, "ID", "Name", creditCard.PassengerID);
            return View(creditCard);
        }

        // GET: CreditCards/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _context.CreditCards
                .Include(c => c.Passenger)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (creditCard == null)
            {
                return NotFound();
            }

            return View(creditCard);
        }

        // POST: CreditCards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var creditCard = await _context.CreditCards.FindAsync(id);
            _context.CreditCards.Remove(creditCard);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CreditCardExists(int id)
        {
            return _context.CreditCards.Any(e => e.ID == id);
        }
    }
}
