﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;

namespace BussyBus.Controllers
{
    public class RoleHasMenusController : Controller
    {
        private readonly BussyBusDBContext _context;

        public RoleHasMenusController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: RoleHasMenus
        public async Task<IActionResult> Index()
        {
            var bussyBusDBContext = _context.RoleHasMenus.Include(r => r.Menu).Include(r => r.Role);
            return View(await bussyBusDBContext.ToListAsync());
        }

        // GET: RoleHasMenus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleHasMenu = await _context.RoleHasMenus
                .Include(r => r.Menu)
                .Include(r => r.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (roleHasMenu == null)
            {
                return NotFound();
            }

            return View(roleHasMenu);
        }

        // GET: RoleHasMenus/Create
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["MenuID"] = new SelectList(_context.Menus, "ID", "Action");
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Name");
            return View();
        }

        // POST: RoleHasMenus/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,RoleID,MenuID")] RoleHasMenu roleHasMenu)
        {
            if (ModelState.IsValid)
            {
                if (roleHasMenu.RoleID == 0 || roleHasMenu.MenuID == 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "RoleHasMenu must have a role and menu selected." });
                }
                _context.Add(roleHasMenu);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MenuID"] = new SelectList(_context.Menus, "ID", "Action", roleHasMenu.MenuID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Name", roleHasMenu.RoleID);
            return View(roleHasMenu);
        }

        // GET: RoleHasMenus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleHasMenu = await _context.RoleHasMenus.FindAsync(id);
            if (roleHasMenu == null)
            {
                return NotFound();
            }
            ViewData["MenuID"] = new SelectList(_context.Menus, "ID", "Action", roleHasMenu.MenuID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Name", roleHasMenu.RoleID);
            return View(roleHasMenu);
        }

        // POST: RoleHasMenus/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,RoleID,MenuID")] RoleHasMenu roleHasMenu)
        {
            if (id != roleHasMenu.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(roleHasMenu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleHasMenuExists(roleHasMenu.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MenuID"] = new SelectList(_context.Menus, "ID", "Action", roleHasMenu.MenuID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Name", roleHasMenu.RoleID);
            return View(roleHasMenu);
        }

        // GET: RoleHasMenus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var roleHasMenu = await _context.RoleHasMenus
                .Include(r => r.Menu)
                .Include(r => r.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (roleHasMenu == null)
            {
                return NotFound();
            }

            return View(roleHasMenu);
        }

        // POST: RoleHasMenus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var roleHasMenu = await _context.RoleHasMenus.FindAsync(id);
            _context.RoleHasMenus.Remove(roleHasMenu);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RoleHasMenuExists(int id)
        {
            return _context.RoleHasMenus.Any(e => e.ID == id);
        }
    }
}
