﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;
using BussyBus.Filter;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdminDriver))]
    public class BusHasDriversController : Controller
    {
        private readonly BussyBusDBContext _context;

        public BusHasDriversController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: BusHasDrivers
        public async Task<IActionResult> Index(string errorMessage, string message, BusHasDriverFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listBusHasDrivers = new List<BusHasDriver>();
            var userId = HttpContext.Session.GetInt32("userId");
            if (nameRole.Name.Equals("DRIVER"))
            {
                var driver = _context.Drivers.Include(b => b.UserAccount).Where(d => d.UserAccountId.Equals(userId)).First();
                listBusHasDrivers = _context.BusHasDrivers.Include(b => b.Driver).Include(b => b.Bus).Where(b => b.Driver.Equals(driver)).ToList();
            }
            else { 
                listBusHasDrivers = _context.BusHasDrivers.Include(b => b.Bus).Include(d => d.Driver).ToList();
            }
            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_DRIVERCODE", "By asceding driver code");
            dictionaryOrdering.Add("DSC_DRIVERCODE", "By desceding driver code");
            dictionaryOrdering.Add("ASC_BUSCODE", "By asceding bus code");
            dictionaryOrdering.Add("DSC_BUSCODE", "By desceding bus code");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<BusHasDriver>(true);
            if (filters.DriverCodeMax != 0)
                predicado = predicado.And(i => i.Driver.Code <= filters.DriverCodeMax);
            if (filters.DriverCodeMin != 0)
                predicado = predicado.And(i => i.Driver.Code >= filters.DriverCodeMin);
            if (filters.BusCodeMax != 0)
                predicado = predicado.And(i => i.Bus.Code <= filters.DriverCodeMax);
            if (filters.BusCodeMin != 0)
                predicado = predicado.And(i => i.Bus.Code >= filters.DriverCodeMin);

            switch (filters.Ordering)
            {
                case "ASC_DRIVERCODE":
                    listBusHasDrivers = listBusHasDrivers.OrderBy(t => t.Driver.Code).ToList();
                    break;
                case "DSC_DRIVERCODE":
                    listBusHasDrivers = listBusHasDrivers.OrderByDescending(t => t.Driver.Code).ToList();
                    break;
                case "ASC_BUSCODE":
                    listBusHasDrivers = listBusHasDrivers.OrderBy(t => t.Bus.Code).ToList();
                    break;
                case "DSC_BUSCODE":
                    listBusHasDrivers = listBusHasDrivers.OrderByDescending(t => t.Bus.Code).ToList();
                    break;

                default:
                    listBusHasDrivers = listBusHasDrivers.OrderBy(t => t.Driver.Code).ToList();
                    break;
            }

            ViewBag.listBusHasDrivers = listBusHasDrivers.Where(predicado);

            return View();
        }

        // GET: BusHasDrivers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasDriver = await _context.BusHasDrivers
                .Include(b => b.Bus)
                .Include(b => b.Driver)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busHasDriver == null)
            {
                return NotFound();
            }

            return View(busHasDriver);
        }

        // GET: BusHasDrivers/Create
        [ServiceFilter(typeof(SecurityAdmin))]
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            ViewData["DriverID"] = new SelectList(_context.Drivers, "ID", "DNI");
            return View();
        }

        // POST: BusHasDrivers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Create([Bind("ID, Code, BusID,DriverID")] BusHasDriver busHasDriver)
        {
            if (ModelState.IsValid)
            {
                ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
                ViewData["DriverID"] = new SelectList(_context.Drivers, "ID", "DNI");
                if (busHasDriver.BusID == 0 || busHasDriver.DriverID == 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "BusHasDriver must have a bus and a driver selected." });
                }
                //comprobamos que el codigo de bus es unico
                var busHasDriverCodeRepeat = _context.BusHasDrivers.Where(b => b.Code == busHasDriver.Code).ToList();
                if (busHasDriverCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "BusHasDriver code already assingned to another busHasDriver." });
                }
                busHasDriver.Driver = _context.Drivers.Where(d => d.ID == busHasDriver.DriverID).First();
                busHasDriver.Bus = _context.Buses.Where(b => b.ID == busHasDriver.BusID).First();
                _context.Add(busHasDriver);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(busHasDriver);
        }

        // GET: BusHasDrivers/Edit/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasDriver = await _context.BusHasDrivers.FindAsync(id);
            if (busHasDriver == null)
            {
                return NotFound();
            }
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "ID", busHasDriver.BusID);
            ViewData["DriverID"] = new SelectList(_context.Drivers, "ID", "DNI", busHasDriver.DriverID);
            return View(busHasDriver);
        }

        // POST: BusHasDrivers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BusID,DriverID")] BusHasDriver busHasDriver)
        {
            if (id != busHasDriver.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(busHasDriver);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BusHasDriverExists(busHasDriver.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "ID", busHasDriver.BusID);
            ViewData["DriverID"] = new SelectList(_context.Drivers, "ID", "DNI", busHasDriver.DriverID);
            return View(busHasDriver);
        }

        // GET: BusHasDrivers/Delete/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasDriver = await _context.BusHasDrivers
                .Include(b => b.Bus)
                .Include(b => b.Driver)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busHasDriver == null)
            {
                return NotFound();
            }

            return View(busHasDriver);
        }

        // POST: BusHasDrivers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var busHasDriver = await _context.BusHasDrivers.FindAsync(id);
            _context.BusHasDrivers.Remove(busHasDriver);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BusHasDriverExists(int id)
        {
            return _context.BusHasDrivers.Any(e => e.ID == id);
        }
    }
}
