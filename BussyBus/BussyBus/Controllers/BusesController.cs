﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdmin))]
    public class BusesController : Controller
    {
        private readonly BussyBusDBContext _context;

        public BusesController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Buses
        public async Task<IActionResult> Index(string errorMessage, string message, BusFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listBuses = new List<Bus>();

            listBuses = _context.Buses.ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_CAPACITY", "By asceding capacity");
            dictionaryOrdering.Add("DSC_CAPACITY", "By desceding capacity");
            dictionaryOrdering.Add("ASC_CODE", "By asceding code");
            dictionaryOrdering.Add("DSC_CODE", "By desceding code");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Bus>(true);
            if (filters.CapacityMax != 0)
                predicado = predicado.And(i => i.Capacity <= filters.CapacityMax);
            if (filters.CapacityMin != 0)
                predicado = predicado.And(i => i.Capacity >= filters.CapacityMin);
            if (filters.CodeMax != 0)
                predicado = predicado.And(i => i.Code <= filters.CodeMax);
            if (filters.CodeMin != 0)
                predicado = predicado.And(i => i.Code >= filters.CodeMin);

            switch (filters.Ordering)
            {
                case "ASC_CAPACITY":
                    listBuses = listBuses.OrderBy(t => t.Capacity).ToList();
                    break;
                case "DSC_CAPACITY":
                    listBuses = listBuses.OrderByDescending(t => t.Capacity).ToList();
                    break;
                case "ASC_CODE":
                    listBuses = listBuses.OrderBy(t => t.Code).ToList();
                    break;
                case "DSC_CODE":
                    listBuses = listBuses.OrderByDescending(t => t.Code).ToList();
                    break;

                default:
                    listBuses = listBuses.OrderBy(t => t.Code).ToList();
                    break;
            }

            ViewBag.listBuses = listBuses.Where(predicado);

            return View();
        }

        // GET: Buses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bus = await _context.Buses
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bus == null)
            {
                return NotFound();
            }

            return View(bus);
        }

        // GET: Buses/Create
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            return View();
        }

        // POST: Buses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Create([Bind("ID,Capacity,Code")] Bus bus)
        {
            if (ModelState.IsValid)
            {
                //comprobamos que el codigo de bus es unico
                var busCodeRepeat = _context.Buses.Where(b => b.Code == bus.Code).ToList();
                if (busCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "Bus code already assingned to another bus." });
                }

                _context.Add(bus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(bus);
        }

        // GET: Buses/Edit/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bus = await _context.Buses.FindAsync(id);
            if (bus == null)
            {
                return NotFound();
            }
            return View(bus);
        }

        // POST: Buses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Capacity,Code")] Bus bus)
        {
            if (id != bus.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BusExists(bus.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bus);
        }

        // GET: Buses/Delete/5
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bus = await _context.Buses
                .FirstOrDefaultAsync(m => m.ID == id);
            if (bus == null)
            {
                return NotFound();
            }

            return View(bus);
        }

        // POST: Buses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bus = await _context.Buses.FindAsync(id);
            var busHasDriver = _context.BusHasDrivers.Include(t => t.Bus).Where(b => b.Bus.ID == id);
            if (bus != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Bus has busHasDriver associated." });
            }
            var busHasStop = _context.BusHasStops.Include(t => t.Bus).Where(b => b.Bus.ID == id);
            if (bus != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Bus has busHasStop associated." });
            }
            var journey = _context.Journeys.Include(j => j.StartBusStop).Include(j => j.EndBusStop).Where(j => j.StartBusStop.ID == id || j.EndBusStop.ID == id);
            if (bus != null)
            {
                return RedirectToAction("Index", new { errorMessage = "Bus has journey associated." });
            }
            _context.Buses.Remove(bus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BusExists(int id)
        {
            return _context.Buses.Any(e => e.ID == id);
        }
    }
}
