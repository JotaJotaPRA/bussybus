﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdmin))]
    public class MenusController : Controller
    {
        private readonly BussyBusDBContext _context;

        public MenusController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Menus
        public async Task<IActionResult> Index(string errorMessage, string message, MenuFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Name");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listMenus = new List<Menu>();
            listMenus = _context.Menus.ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_CONTROLLER", "By ascending controller");
            dictionaryOrdering.Add("DSC_CONTROLLER", "By descending controller");
            dictionaryOrdering.Add("ASC_ACTION", "By ascending action");
            dictionaryOrdering.Add("DSC_ACTION", "By descending action");
            dictionaryOrdering.Add("ASC_LABEL", "By ascending label");
            dictionaryOrdering.Add("DSC_LABEL", "By descending label");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Menu>(true);
            if (!string.IsNullOrEmpty(filters.Controller))
                predicado = predicado.And(i => i.Controller.Contains(filters.Controller));
            if (!string.IsNullOrEmpty(filters.Action))
                predicado = predicado.And(i => i.Action.Contains(filters.Action));
            if (!string.IsNullOrEmpty(filters.Label))
                predicado = predicado.And(i => i.Label.Contains(filters.Label));

            switch (filters.Ordering)
            {
                case "ASC_CONTROLLER":
                    listMenus = listMenus.OrderBy(t => t.Controller).ToList();
                    break;
                case "DSC_CONTROLLER":
                    listMenus = listMenus.OrderByDescending(t => t.Controller).ToList();
                    break;
                case "ASC_ACTION":
                    listMenus = listMenus.OrderBy(t => t.Action).ToList();
                    break;
                case "DSC_ACTION":
                    listMenus = listMenus.OrderByDescending(t => t.Action).ToList();
                    break;
                case "ASC_LABEL":
                    listMenus = listMenus.OrderBy(t => t.Label).ToList();
                    break;
                case "DSC_LABEL":
                    listMenus = listMenus.OrderByDescending(t => t.Label).ToList();
                    break;
            }

            ViewBag.listMenus = listMenus.Where(predicado);
            return View();
        }

        // GET: Menus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var menu = await _context.Menus
                .FirstOrDefaultAsync(m => m.ID == id);
            if (menu == null)
            {
                return NotFound();
            }

            return View(menu);
        }

        // GET: Menus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Menus/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Controller,Action,Label")] Menu menu)
        {
            if (ModelState.IsValid)
            {
                _context.Add(menu);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(menu);
        }

        // GET: Menus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var menu = await _context.Menus.FindAsync(id);
            if (menu == null)
            {
                return NotFound();
            }
            return View(menu);
        }

        // POST: Menus/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Controller,Action,Label")] Menu menu)
        {
            if (id != menu.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(menu);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MenuExists(menu.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(menu);
        }

        // GET: Menus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var menu = await _context.Menus
                .FirstOrDefaultAsync(m => m.ID == id);
            if (menu == null)
            {
                return NotFound();
            }

            return View(menu);
        }

        // POST: Menus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var roleHasMenu = _context.RoleHasMenus.Include(r => r.Menu).Where(r => r.MenuID == id);
            if(roleHasMenu != null){
                return RedirectToAction("Index", new { errorMessage = "Menu has roleHasMenu associated." });
            }
            var menu = await _context.Menus.FindAsync(id);
            _context.Menus.Remove(menu);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MenuExists(int id)
        {
            return _context.Menus.Any(e => e.ID == id);
        }
    }
}
