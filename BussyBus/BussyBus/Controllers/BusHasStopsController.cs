﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Filter;
using BussyBus.Dto;
using Microsoft.AspNetCore.Http;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdPasDri))]
    public class BusHasStopsController : Controller
    {
        private readonly BussyBusDBContext _context;

        public BusHasStopsController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: BusHasStops
        public async Task<IActionResult> Index(string errorMessage, string message, BusHasStopFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listBusHasStops = new List<BusHasStop>();

            listBusHasStops = _context.BusHasStops.Include(b => b.Bus).Include(b => b.BusStop).ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_BUSCODE", "By asceding bus code");
            dictionaryOrdering.Add("DSC_BUSCODE", "By desceding bus code");
            dictionaryOrdering.Add("ASC_STOPNUMBER", "By asceding stop number");
            dictionaryOrdering.Add("DSC_STOPNUMBER", "By desceding stop number");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<BusHasStop>(true);
            if (filters.BusCodeMax != 0)
                predicado = predicado.And(i => i.Bus.Code <= filters.BusCodeMax);
            if (filters.BusCodeMin != 0)
                predicado = predicado.And(i => i.Bus.Code >= filters.BusCodeMin);
            if (filters.StopNumberMax != 0)
                predicado = predicado.And(i => i.BusStop.Number <= filters.StopNumberMax);
            if (filters.StopNumberMin != 0)
                predicado = predicado.And(i => i.BusStop.Number >= filters.StopNumberMin);

            switch (filters.Ordering)
            {
                case "ASC_BUSCODE":
                    listBusHasStops = listBusHasStops.OrderBy(t => t.Bus.Code).ToList();
                    break;
                case "DSC_BUSCODE":
                    listBusHasStops = listBusHasStops.OrderByDescending(t => t.Bus.Code).ToList();
                    break;
                case "ASC_STOPNUMBER":
                    listBusHasStops = listBusHasStops.OrderBy(t => t.BusStop.Number).ToList();
                    break;
                case "DSC_STOPNUMBER":
                    listBusHasStops = listBusHasStops.OrderByDescending(t => t.BusStop.Number).ToList();
                    break;

                default:
                    listBusHasStops = listBusHasStops.OrderBy(t => t.BusStop.Number).ToList();
                    break;
            }

            ViewBag.listBusHasStops = listBusHasStops.Where(predicado);

            return View();
            
        }

        // GET: BusHasStops/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasStop = await _context.BusHasStops
                .Include(b => b.Bus)
                .Include(b => b.BusStop)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busHasStop == null)
            {
                return NotFound();
            }

            return View(busHasStop);
        }
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]

        // GET: BusHasStops/Create
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code");
            ViewData["StopID"] = new SelectList(_context.BusStops, "ID", "Direction");
            return View();
        }

        // POST: BusHasStops/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID, Code, BusID,StopID")] BusHasStop busHasStop)
        {
            busHasStop.Bus = _context.Buses.Where(b => b.ID == busHasStop.BusID).ToList().First();
            busHasStop.BusStop = _context.BusStops.Where(b => b.ID == busHasStop.StopID).ToList().First();
            if (ModelState.IsValid)
            {
                if(busHasStop.BusID == 0 || busHasStop.StopID == 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "BusHasStop must have a bus and bus stop selected." });
                }
                //comprobamos que el codigo de busHasStop es unico
                var busHasStopCodeRepeat = _context.BusHasStops.Where(b => b.Code == busHasStop.Code).ToList();
                if (busHasStopCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "BusHasStop code already assingned to another busHasStop." });
                }
                //comprobamos que bus tiene driver
                List<BusHasDriver> listBusHasDriver = _context.BusHasDrivers.Where(d => d.BusID == busHasStop.BusID).ToList();
                if (listBusHasDriver.Count == 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "Bus doesn´t have a driver assingned." });
                }
                

                _context.Add(busHasStop);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "Code", busHasStop.BusID);
            ViewData["StopID"] = new SelectList(_context.BusStops, "ID", "Direction", busHasStop.StopID);
            return View(busHasStop);
        }

        // GET: BusHasStops/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasStop = await _context.BusHasStops.FindAsync(id);
            if (busHasStop == null)
            {
                return NotFound();
            }
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "ID", busHasStop.BusID);
            ViewData["StopID"] = new SelectList(_context.BusStops, "ID", "Direction", busHasStop.StopID);
            return View(busHasStop);
        }

        // POST: BusHasStops/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,BusID,StopID")] BusHasStop busHasStop)
        {
            if (id != busHasStop.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(busHasStop);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BusHasStopExists(busHasStop.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BusID"] = new SelectList(_context.Buses, "ID", "ID", busHasStop.BusID);
            ViewData["StopID"] = new SelectList(_context.BusStops, "ID", "Direction", busHasStop.StopID);
            return View(busHasStop);
        }

        // GET: BusHasStops/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var busHasStop = await _context.BusHasStops
                .Include(b => b.Bus)
                .Include(b => b.BusStop)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (busHasStop == null)
            {
                return NotFound();
            }

            return View(busHasStop);
        }

        // POST: BusHasStops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var busHasStop = await _context.BusHasStops.FindAsync(id);
            _context.BusHasStops.Remove(busHasStop);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BusHasStopExists(int id)
        {
            return _context.BusHasStops.Any(e => e.ID == id);
        }
    }
}
