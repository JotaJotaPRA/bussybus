﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;
using BussyBus.Dto;
using BussyBus.Utilities;
using Microsoft.AspNetCore.Http;
using BussyBus.Filter;
using LinqKit;

namespace BussyBus.Controllers
{
    [ServiceFilter(typeof(Security))]
    [ServiceFilter(typeof(SecurityAdminDriver))]
    public class DriversController : Controller
    {
        private readonly BussyBusDBContext _context;

        public DriversController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: Drivers
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Index(string errorMessage, string message, DriverFilterDto filters)
        {
            ViewBag.errorMessage = errorMessage;
            ViewBag.message = message;

            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Name");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userAccountId = HttpContext.Session.GetString("user");

            var listDrivers = new List<Driver>();
            listDrivers = _context.Drivers.Include(u => u.UserAccount).ToList();

            Dictionary<string, string> dictionaryOrdering = new Dictionary<string, string>();
            dictionaryOrdering.Add("ASC_CODE", "By ascending code");
            dictionaryOrdering.Add("DSC_CODE", "By descending code");
            dictionaryOrdering.Add("ASC_NAME", "By ascending name");
            dictionaryOrdering.Add("DSC_NAME", "By descending name");
            dictionaryOrdering.Add("ASC_SURNAME", "By ascending surname");
            dictionaryOrdering.Add("DSC_SURNAME", "By descending surname");
            dictionaryOrdering.Add("ASC_PHONE", "By ascending phone");
            dictionaryOrdering.Add("DSC_PHONE", "By descending phone");
            dictionaryOrdering.Add("ASC_USERNAME", "By ascending username");
            dictionaryOrdering.Add("DSC_USERNAME", "By descending username");
            dictionaryOrdering.Add("ASC_NSS", "By ascending NSS");
            dictionaryOrdering.Add("DSC_NSS", "By descending NSS");
            dictionaryOrdering.Add("ASC_DNI", "By ascending DNI");
            dictionaryOrdering.Add("DSC_DNI", "By descending DNI");
            dictionaryOrdering.Add("ASC_DIRECTION", "By ascending direction");
            dictionaryOrdering.Add("DSC_DIRECTION", "By descending direction");
            dictionaryOrdering.Add("ASC_NUMBER", "By ascending number");
            dictionaryOrdering.Add("DSC_NUMBER", "By descending number");
            dictionaryOrdering.Add("ASC_ADDITIONALINFO", "By ascending additional info");
            dictionaryOrdering.Add("DSC_ADDITIONALINFO", "By descending additional info");

            ViewBag.orderingList = new SelectList(dictionaryOrdering, "Key", "Value");

            //Predicado       
            var predicado = PredicateBuilder.New<Driver>(true);
            if (filters.CodeMax != 0)
                predicado = predicado.And(i => i.Code <= filters.CodeMax);
            if (filters.CodeMin != 0)
                predicado = predicado.And(i => i.Code >= filters.CodeMin);
            if (!string.IsNullOrEmpty(filters.Name))
                predicado = predicado.And(i => i.Name.Contains(filters.Name));
            if (!string.IsNullOrEmpty(filters.Surname))
                predicado = predicado.And(i => i.Surname.Contains(filters.Surname));
            if (!string.IsNullOrEmpty(filters.Phone))
                predicado = predicado.And(i => i.Phone.Contains(filters.Phone));
            if (!string.IsNullOrEmpty(filters.Username))
                predicado = predicado.And(i => i.UserAccount.Username.Contains(filters.Username));
            if (!string.IsNullOrEmpty(filters.NSS))
                predicado = predicado.And(i => i.NSS.Contains(filters.NSS));
            if (!string.IsNullOrEmpty(filters.DNI))
                predicado = predicado.And(i => i.DNI.Contains(filters.DNI));
            if (!string.IsNullOrEmpty(filters.Direction))
                predicado = predicado.And(i => i.Direction.Contains(filters.Direction));
            if (filters.Code != 0)
                predicado = predicado.And(i => i.Code == filters.Code);
            if (!string.IsNullOrEmpty(filters.AdditionalInfo))
                predicado = predicado.And(i => i.AdditionalInfo.Contains(filters.AdditionalInfo));

            switch (filters.Ordering)
            {
                case "ASC_CODE":
                    listDrivers = listDrivers.OrderBy(t => t.Code).ToList();
                    break;
                case "DSC_CODE":
                    listDrivers = listDrivers.OrderByDescending(t => t.Code).ToList();
                    break;
                case "ASC_NAME":
                    listDrivers = listDrivers.OrderBy(t => t.Name).ToList();
                    break;
                case "DSC_NAME":
                    listDrivers = listDrivers.OrderByDescending(t => t.Name).ToList();
                    break;
                case "ASC_SURNAME":
                    listDrivers = listDrivers.OrderBy(t => t.Surname).ToList();
                    break;
                case "DSC_SURNAME":
                    listDrivers = listDrivers.OrderByDescending(t => t.Surname).ToList();
                    break;
                case "ASC_PHONE":
                    listDrivers = listDrivers.OrderBy(t => t.Phone).ToList();
                    break;
                case "DSC_PHONE":
                    listDrivers = listDrivers.OrderByDescending(t => t.Phone).ToList();
                    break;
                case "ASC_USERNAME":
                    listDrivers = listDrivers.OrderBy(t => t.UserAccount.Username).ToList();
                    break;
                case "DSC_USERNAME":
                    listDrivers = listDrivers.OrderByDescending(t => t.UserAccount.Username).ToList();
                    break;
                case "ASC_NSS":
                    listDrivers = listDrivers.OrderBy(t => t.NSS).ToList();
                    break;
                case "DSC_NSS":
                    listDrivers = listDrivers.OrderByDescending(t => t.NSS).ToList();
                    break;
                case "ASC_DNI":
                    listDrivers = listDrivers.OrderBy(t => t.DNI).ToList();
                    break;
                case "DSC_DNI":
                    listDrivers = listDrivers.OrderByDescending(t => t.DNI).ToList();
                    break;
                case "ASC_DIRECTION":
                    listDrivers = listDrivers.OrderBy(t => t.Direction).ToList();
                    break;
                case "DSC_DIRECTION":
                    listDrivers = listDrivers.OrderByDescending(t => t.Direction).ToList();
                    break;
                case "ASC_ADDITIONALINFO":
                    listDrivers = listDrivers.OrderBy(t => t.AdditionalInfo).ToList();
                    break;
                case "DSC_ADDITIONALINFO":
                    listDrivers = listDrivers.OrderByDescending(t => t.AdditionalInfo).ToList();
                    break;
                default:
                    listDrivers = listDrivers.OrderBy(t => t.Name).ToList();
                    break;
            }

            ViewBag.listDrivers = listDrivers.Where(predicado);

            return View();
        }

        // GET: Drivers/Details/5
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdminDriver))]
        public async Task<IActionResult> Details(int? id)
        {
            var userId = HttpContext.Session.GetInt32("userId");
            var userRole = HttpContext.Session.GetString("userRole");
            var nameRole = _context.Roles.Where(c => c.ID.ToString().Equals(userRole)).FirstOrDefault();
            ViewBag.role = nameRole.Name;
            var userDBContext = _context.Drivers.Include(c => c.UserAccount);
            var driver = userDBContext.First();
            if (nameRole.Name.Equals("ADMIN"))
            {
                driver = userDBContext.Where(a => a.ID.Equals(id)).First();
            }
            else
            {
                driver = userDBContext.Where(u => u.UserAccountId.Equals(userId)).First();
                
            }
            var driverAccountDBContext = _context.UserAccounts;
            var driverAccount = driverAccountDBContext.Where(u => u.ID.Equals(userId)).First();

            DriverDto driverDto = new DriverDto
            {
                ID = driver.UserAccountId,
                Username = driver.UserAccount.Username,
                Password = driver.UserAccount.Password,
                ConfirmPassword = driver.UserAccount.Password,
                Email = driver.UserAccount.Email,
                Name = driver.Name,
                Surname = driver.Surname,
                Phone = driver.Phone,
                Active = driver.UserAccount.Active,
                NSS = driver.NSS,
                DNI = driver.DNI,
                Direction = driver.Direction,
                Code = driver.Code,
                AdditionalInfo = driver.AdditionalInfo

            };

            return View(driverDto);
        }

        // GET: Drivers/Create
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]
        public IActionResult Create(string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            ViewData["UserAccountId"] = new SelectList(_context.UserAccounts, "ID", "Password");
            return View();
        }

        // POST: Drivers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityAdmin))]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone, NSS,DNI,EmployeeCode,Direction,Number,Code,AdditionalInfo")] DriverDto driverDto)
        {
            if (ModelState.IsValid)
            {


                if (driverDto.Password != driverDto.ConfirmPassword)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "error" });
                }

                var mismoUsuario = _context.Admins.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username.Equals(driverDto.Username) || u.UserAccount.Email.Equals(driverDto.Email)).ToList();
                var mismoUs = _context.Passengers.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username == driverDto.Username || u.UserAccount.Email == driverDto.Email).ToList();
                var mismoUs2 = _context.Drivers.Include(u => u.UserAccount)
                    .Where(u => u.UserAccount.Username == driverDto.Username || u.UserAccount.Email == driverDto.Email).ToList();

                if (mismoUsuario.Count != 0 || mismoUs.Count != 0 || mismoUs2.Count != 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "account username already created" });
                }
                var role = _context.Roles.Where(r => r.Name.Equals("DRIVER")).First();

                //comprobamos que el codigo de driver es unico
                var driverCodeRepeat = _context.Drivers.Include(u => u.UserAccount).Where(a => a.Code == driverDto.Code).ToList();
                if (driverCodeRepeat.Count > 0)
                {
                    return RedirectToAction(nameof(Create), new { errorMessage = "Driver code already assingned to another driver." });
                }
                //create userAcount
                UserAccount userAccount = new UserAccount();
                userAccount.Username = driverDto.Username;
                userAccount.Password = driverDto.Password;
                userAccount.RoleId = role.ID;
                userAccount.Email = driverDto.Email;
                userAccount.Active = true;

                userAccount.Password = Utility.Encrypt(userAccount.Password);

                _context.UserAccounts.Add(userAccount);
                await _context.SaveChangesAsync();

                //create driver
                Driver driver = new Driver();
                driver.UserAccountId = userAccount.ID; //esto hace referencia al user account que hemos creado
                driver.Name = driverDto.Name;
                driver.Surname = driverDto.Surname;
                driver.Phone = driverDto.Phone;
                driver.NSS = driverDto.NSS;
                driver.DNI = driverDto.DNI;
                driver.Direction = driverDto.Direction;
                driver.Code = driverDto.Code;
                driver.AdditionalInfo = driverDto.AdditionalInfo;

                _context.Drivers.Add(driver);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Drivers");

            }
            return View(driverDto);
        }

        // GET: Drivers/Edit/5
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityDriver))]
        public async Task<IActionResult> Edit(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var user = _context.Passengers.Include(c => c.UserAccount)
                .Where(c => c.UserAccountId == userId).FirstOrDefault();
            var driverDBContext = _context.Drivers.Include(c => c.UserAccount);
            var driver = driverDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            var driverAccountDBContext = _context.UserAccounts;
            var driverAccount = driverAccountDBContext.Where(u => u.ID.Equals(userId)).First();

            DriverDto driverDto = new DriverDto
            {
                ID = driver.UserAccountId,
                Username = driver.UserAccount.Username,
                Password = driver.UserAccount.Password,
                ConfirmPassword = driver.UserAccount.Password,
                Email = driver.UserAccount.Email,
                Name = driver.Name,
                Surname = driver.Surname,
                Phone = driver.Phone,
                Active = driver.UserAccount.Active,
                NSS = driver.NSS,
                DNI = driver.DNI,
                Direction = driver.Direction,
                Code = driver.Code,
                AdditionalInfo = driver.AdditionalInfo

            };

            return View(driverDto);
        }

        // POST: Drivers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ServiceFilter(typeof(Security))]
        [ServiceFilter(typeof(SecurityDriver))]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Username,Password,ConfirmPassword,Email,Active,Name,Surname,Phone, NSS,DNI,EmployeeCode,Direction,Number,AdditionalInfo")] DriverDto driverDto)
        {
            if (id != driverDto.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (id != driverDto.ID)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        var userId = HttpContext.Session.GetInt32("userId");
                        var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                        if (b.Password.Equals(Utility.Encrypt(driverDto.Password)) && driverDto.Password.Equals(driverDto.ConfirmPassword))
                        {
                            //modificamos el useraccount
                            var role = _context.Roles.Where(r => r.Name.Equals("DRIVER")).First();

                            b.ID = (int)userId;
                            b.Username = driverDto.Username;
                            b.Email = driverDto.Email;
                            b.Active = driverDto.Active;
                            b.RoleId = role.ID;

                            _context.UserAccounts.Update(b);
                            await _context.SaveChangesAsync();

                            //modificamos el driver
                            var d = _context.Drivers.Where(c => c.UserAccountId.Equals(driverDto.ID)).First();
                            d.Name = driverDto.Name;
                            d.Surname = driverDto.Surname;
                            d.Phone = driverDto.Phone;
                            d.NSS = driverDto.NSS;
                            d.DNI = driverDto.DNI;
                            d.Code = driverDto.Code;
                            d.Direction = driverDto.Direction;
                            d.Code = driverDto.Code;
                            d.AdditionalInfo = driverDto.AdditionalInfo;
                            d.UserAccountId = (int)userId;

                            _context.Drivers.Update(d);
                            await _context.SaveChangesAsync();
                        }
                        else
                        {
                            return RedirectToAction("Index", "Home", new { errorMessage = "Incorrect password" });
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        throw;
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(driverDto);
        }

        

        private bool DriverExists(int id)
        {
            return _context.Drivers.Any(e => e.ID == id);
        }
        public async Task<IActionResult> ChangePassword(int? id, string errorMessage)
        {
            ViewBag.errorMessage = errorMessage;
            var userId = HttpContext.Session.GetInt32("userId");
            var driverDBContext = _context.Drivers.Include(c => c.UserAccount);
            var driver = driverDBContext.Where(a => a.UserAccountId.Equals(userId)).First();

            PasswordProfileDto passwordProfileDto = new PasswordProfileDto();
            passwordProfileDto.ID = driver.ID;
            passwordProfileDto.OldPassword = driver.UserAccount.Password;


            return View(passwordProfileDto);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(int id, [Bind("ID,Email,OldPassword,Password,ConfirmPassword")] PasswordProfileDto passwordProfileDto)
        {
            if (id != passwordProfileDto.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userId = HttpContext.Session.GetInt32("userId");
                    var b = _context.UserAccounts.Where(c => c.ID.Equals(userId)).First();
                    //Comprobamos que Oldpassword sea correcta
                    if (!Utility.Encrypt(passwordProfileDto.OldPassword).Equals(b.Password))
                    {
                        return RedirectToAction("ChangePassword", "Drivers", new { errorMessage = "Incorrect password" });
                    }
                    //Comprobamos que password y ConfirmPassword sean iguales
                    if (passwordProfileDto.Password.Equals(passwordProfileDto.ConfirmPassword))
                    {
                        //modificamos el useraccount

                        b.ID = (int)userId;
                        b.Password = Utility.Encrypt(passwordProfileDto.Password);

                        _context.UserAccounts.Update(b);
                        await _context.SaveChangesAsync();

                    }
                    else
                    {
                        return RedirectToAction("ChangePassword", "Drivers", new { errorMessage = "New password and confirm password not the same" });
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", "Drivers", new { errorMessage = "Password changed" });
            }
            return View(passwordProfileDto);
        }
    }
}
