﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BussyBus.Data;
using BussyBus.Models;

namespace BussyBus.Controllers
{
    public class TicketHasJourneysController : Controller
    {
        private readonly BussyBusDBContext _context;

        public TicketHasJourneysController(BussyBusDBContext context)
        {
            _context = context;
        }

        // GET: TicketHasJourneys
        public async Task<IActionResult> Index()
        {
            var bussyBusDBContext = _context.TicketHasJourney.Include(t => t.Journey).Include(t => t.Ticket);
            return View(await bussyBusDBContext.ToListAsync());
        }

        // GET: TicketHasJourneys/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticketHasJourney = await _context.TicketHasJourney
                .Include(t => t.Journey)
                .Include(t => t.Ticket)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (ticketHasJourney == null)
            {
                return NotFound();
            }

            return View(ticketHasJourney);
        }

        // GET: TicketHasJourneys/Create
        public IActionResult Create()
        {
            ViewData["JourneyID"] = new SelectList(_context.Journeys, "ID", "ID");
            ViewData["TicketID"] = new SelectList(_context.Tickets, "ID", "ID");
            return View();
        }

        // POST: TicketHasJourneys/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,JourneyID,TicketID")] TicketHasJourney ticketHasJourney)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ticketHasJourney);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["JourneyID"] = new SelectList(_context.Journeys, "ID", "ID", ticketHasJourney.JourneyID);
            ViewData["TicketID"] = new SelectList(_context.Tickets, "ID", "ID", ticketHasJourney.TicketID);
            return View(ticketHasJourney);
        }

        // GET: TicketHasJourneys/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticketHasJourney = await _context.TicketHasJourney.FindAsync(id);
            if (ticketHasJourney == null)
            {
                return NotFound();
            }
            ViewData["JourneyID"] = new SelectList(_context.Journeys, "ID", "ID", ticketHasJourney.JourneyID);
            ViewData["TicketID"] = new SelectList(_context.Tickets, "ID", "ID", ticketHasJourney.TicketID);
            return View(ticketHasJourney);
        }

        // POST: TicketHasJourneys/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,JourneyID,TicketID")] TicketHasJourney ticketHasJourney)
        {
            if (id != ticketHasJourney.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ticketHasJourney);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TicketHasJourneyExists(ticketHasJourney.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["JourneyID"] = new SelectList(_context.Journeys, "ID", "ID", ticketHasJourney.JourneyID);
            ViewData["TicketID"] = new SelectList(_context.Tickets, "ID", "ID", ticketHasJourney.TicketID);
            return View(ticketHasJourney);
        }

        // GET: TicketHasJourneys/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var ticketHasJourney = await _context.TicketHasJourney
                .Include(t => t.Journey)
                .Include(t => t.Ticket)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (ticketHasJourney == null)
            {
                return NotFound();
            }

            return View(ticketHasJourney);
        }

        // POST: TicketHasJourneys/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var ticketHasJourney = await _context.TicketHasJourney.FindAsync(id);
            _context.TicketHasJourney.Remove(ticketHasJourney);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TicketHasJourneyExists(int id)
        {
            return _context.TicketHasJourney.Any(e => e.ID == id);
        }
    }
}
