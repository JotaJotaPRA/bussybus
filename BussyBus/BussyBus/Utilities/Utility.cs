﻿using BussyBus.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BussyBus.Utilities
{
    public class Utility
    {
        public static List<Menu> Menus { get; set; } = new List<Menu>();

        public static int TTL = 60;

        public static void SendEmail(string emailTo, string subject, string body)
        {
            MailMessage email = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            email.To.Add(new MailAddress(emailTo));
            email.From = new MailAddress("altairgym2020@gmail.com");
            email.Subject = subject;
            email.SubjectEncoding = System.Text.Encoding.UTF8;
            email.Body = body;
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("altairgym2020@gmail.com", "alaplaya");

            smtp.Send(email);
            email.Dispose();
        }

        public static byte[] GetByteArrayFromImage(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }

        //Encrypt
        public static string Encrypt(string pass)
        {
            SHA256 sha256 = SHA256Managed.Create();
            byte[] hashValue;
            UTF8Encoding objUtf8 = new UTF8Encoding();
            hashValue = sha256.ComputeHash(objUtf8.GetBytes(pass));
            string result = Encoding.Default.GetString(hashValue);
            return result;
        }
    }
}
