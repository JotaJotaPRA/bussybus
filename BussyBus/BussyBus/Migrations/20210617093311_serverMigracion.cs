﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BussyBus.Migrations
{
    public partial class serverMigracion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BUS",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Capacity = table.Column<int>(type: "int", nullable: false),
                    Code = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUS", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "BUSSTOP",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Number = table.Column<int>(type: "int", nullable: false),
                    Direction = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUSSTOP", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "MENU",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Controller = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Action = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Label = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MENU", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PasswordProfileDto",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OldPassword = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ConfirmPassword = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PasswordProfileDto", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ROLE",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ROLE", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "BUSHASSTOP",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<int>(type: "int", nullable: false),
                    BusID = table.Column<int>(type: "int", nullable: false),
                    StopID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUSHASSTOP", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BUSHASSTOP_BUS_BusID",
                        column: x => x.BusID,
                        principalTable: "BUS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BUSHASSTOP_BUSSTOP_StopID",
                        column: x => x.StopID,
                        principalTable: "BUSSTOP",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JOURNEY",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    JourneyDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StartBusStopID = table.Column<int>(type: "int", nullable: false),
                    EndBusStopID = table.Column<int>(type: "int", nullable: true),
                    BusID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JOURNEY", x => x.ID);
                    table.ForeignKey(
                        name: "FK_JOURNEY_BUS_BusID",
                        column: x => x.BusID,
                        principalTable: "BUS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JOURNEY_BUSSTOP_EndBusStopID",
                        column: x => x.EndBusStopID,
                        principalTable: "BUSSTOP",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JOURNEY_BUSSTOP_StartBusStopID",
                        column: x => x.StartBusStopID,
                        principalTable: "BUSSTOP",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ROLEHASMENU",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleID = table.Column<int>(type: "int", nullable: false),
                    MenuID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ROLEHASMENU", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ROLEHASMENU_MENU_MenuID",
                        column: x => x.MenuID,
                        principalTable: "MENU",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ROLEHASMENU_ROLE_RoleID",
                        column: x => x.RoleID,
                        principalTable: "ROLE",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "USERACCOUNT",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USERACCOUNT", x => x.ID);
                    table.ForeignKey(
                        name: "FK_USERACCOUNT_ROLE_RoleId",
                        column: x => x.RoleId,
                        principalTable: "ROLE",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ADMIN",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    UserAccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ADMIN", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ADMIN_USERACCOUNT_UserAccountId",
                        column: x => x.UserAccountId,
                        principalTable: "USERACCOUNT",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "DRIVER",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NSS = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DNI = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Code = table.Column<int>(type: "int", nullable: false),
                    Direction = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    AdditionalInfo = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    UserAccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DRIVER", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DRIVER_USERACCOUNT_UserAccountId",
                        column: x => x.UserAccountId,
                        principalTable: "USERACCOUNT",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "PASSENGER",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    Surname = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(13)", maxLength: 13, nullable: false),
                    UserAccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PASSENGER", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PASSENGER_USERACCOUNT_UserAccountId",
                        column: x => x.UserAccountId,
                        principalTable: "USERACCOUNT",
                        principalColumn: "ID");
                });

            migrationBuilder.CreateTable(
                name: "USER_TOKEN",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Token = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    GenerationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Life = table.Column<int>(type: "int", nullable: false),
                    UserAccountId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_USER_TOKEN", x => x.ID);
                    table.ForeignKey(
                        name: "FK_USER_TOKEN_USERACCOUNT_UserAccountId",
                        column: x => x.UserAccountId,
                        principalTable: "USERACCOUNT",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BUSHASDRIVER",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<int>(type: "int", nullable: false),
                    BusID = table.Column<int>(type: "int", nullable: false),
                    DriverID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BUSHASDRIVER", x => x.ID);
                    table.ForeignKey(
                        name: "FK_BUSHASDRIVER_BUS_BusID",
                        column: x => x.BusID,
                        principalTable: "BUS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BUSHASDRIVER_DRIVER_DriverID",
                        column: x => x.DriverID,
                        principalTable: "DRIVER",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CREDITCARD",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    CreditCardNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CVV = table.Column<string>(type: "nvarchar(3)", maxLength: 3, nullable: false),
                    ExpirationMonth = table.Column<int>(type: "int", nullable: false),
                    ExpirationYear = table.Column<int>(type: "int", nullable: false),
                    PassengerID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CREDITCARD", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CREDITCARD_PASSENGER_PassengerID",
                        column: x => x.PassengerID,
                        principalTable: "PASSENGER",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TICKET",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StartBusStop = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EndBusStop = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Total = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PassengerID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TICKET", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TICKET_PASSENGER_PassengerID",
                        column: x => x.PassengerID,
                        principalTable: "PASSENGER",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TICKETHASJOURNEY",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    JourneyID = table.Column<int>(type: "int", nullable: false),
                    TicketID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TICKETHASJOURNEY", x => x.ID);
                    table.ForeignKey(
                        name: "FK_TICKETHASJOURNEY_JOURNEY_JourneyID",
                        column: x => x.JourneyID,
                        principalTable: "JOURNEY",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TICKETHASJOURNEY_TICKET_TicketID",
                        column: x => x.TicketID,
                        principalTable: "TICKET",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "MENU",
                columns: new[] { "ID", "Action", "Controller", "Label" },
                values: new object[,]
                {
                    { 1, "Index", "Passengers", "Passengers" },
                    { 18, "Index", "TicketHasJourneys", "TicketHasJourneys" },
                    { 17, "Details", "Passengers", "Profile" },
                    { 16, "Details", "Drivers", "Profile" },
                    { 15, "Details", "Admins", "Profile" },
                    { 14, "Index", "UserAccounts", "UserAccount" },
                    { 13, "Index", "Tickets", "Ticket" },
                    { 12, "Index", "RoleHasMenus", "RoleHasMenu" },
                    { 10, "Index", "Menus", "Menu" },
                    { 11, "Index", "Roles", "Role" },
                    { 8, "Index", "CreditCards", "CreditCard" },
                    { 7, "Index", "BusStops", "BusStop" },
                    { 6, "Index", "BusHasStops", "BusHasStop" },
                    { 5, "Index", "BusHasDrivers", "BusHasDriver" },
                    { 4, "Index", "Buses", "Bus" },
                    { 3, "Index", "Admins", "Admins" },
                    { 2, "Index", "Drivers", "Drivers" },
                    { 9, "Index", "Journeys", "Journey" }
                });

            migrationBuilder.InsertData(
                table: "ROLE",
                columns: new[] { "ID", "Description", "Name" },
                values: new object[,]
                {
                    { 2, null, "DRIVER" },
                    { 1, null, "ADMIN" },
                    { 3, null, "PASSENGER" }
                });

            migrationBuilder.InsertData(
                table: "ROLEHASMENU",
                columns: new[] { "ID", "MenuID", "RoleID" },
                values: new object[,]
                {
                    { 1, 2, 1 },
                    { 24, 17, 3 },
                    { 23, 9, 3 },
                    { 22, 13, 3 },
                    { 21, 8, 3 },
                    { 19, 16, 2 },
                    { 18, 9, 2 },
                    { 16, 5, 2 },
                    { 15, 18, 1 },
                    { 13, 15, 1 },
                    { 12, 10, 1 },
                    { 14, 5, 1 },
                    { 10, 14, 1 },
                    { 9, 9, 1 },
                    { 8, 13, 1 },
                    { 7, 12, 1 },
                    { 6, 7, 1 },
                    { 5, 6, 1 },
                    { 4, 4, 1 },
                    { 3, 1, 1 },
                    { 2, 3, 1 },
                    { 11, 11, 1 }
                });

            migrationBuilder.InsertData(
                table: "USERACCOUNT",
                columns: new[] { "ID", "Active", "Email", "Password", "RoleId", "Username" },
                values: new object[,]
                {
                    { 5, true, "passenger1@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 3, "passenger1" },
                    { 1, true, "admin1@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 1, "admin1" },
                    { 2, false, "admin2@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 1, "admin2" },
                    { 3, true, "driver1@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 2, "driver1" },
                    { 4, false, "driver2@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 2, "driver2" },
                    { 6, false, "passenger2@gmail.com", "����H�z*eO�~\\��[֊(p\r��7��", 3, "passenger2" }
                });

            migrationBuilder.InsertData(
                table: "ADMIN",
                columns: new[] { "ID", "Code", "Name", "Phone", "Surname", "UserAccountId" },
                values: new object[,]
                {
                    { 1, 101, "admin1", "611111111", "admin1", 1 },
                    { 2, 102, "admin2", "622222222", "admin2", 2 }
                });

            migrationBuilder.InsertData(
                table: "DRIVER",
                columns: new[] { "ID", "AdditionalInfo", "Code", "DNI", "Direction", "NSS", "Name", "Phone", "Surname", "UserAccountId" },
                values: new object[,]
                {
                    { 1, null, 101, "12345678A", "Calle Balonmano", "12345678901", "driver1", "655555555", "driver1", 3 },
                    { 2, null, 102, "12345679B", "Calle Balonmano", "12345678902", "driver2", "666666667", "driver2", 4 }
                });

            migrationBuilder.InsertData(
                table: "PASSENGER",
                columns: new[] { "ID", "Code", "Name", "Phone", "Surname", "UserAccountId" },
                values: new object[,]
                {
                    { 1, 101, "passenger1", "633333333", "passenger1", 5 },
                    { 2, 102, "passenger2", "644444444", "passenger2", 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ADMIN_UserAccountId",
                table: "ADMIN",
                column: "UserAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_BUSHASDRIVER_BusID",
                table: "BUSHASDRIVER",
                column: "BusID");

            migrationBuilder.CreateIndex(
                name: "IX_BUSHASDRIVER_DriverID",
                table: "BUSHASDRIVER",
                column: "DriverID");

            migrationBuilder.CreateIndex(
                name: "IX_BUSHASSTOP_BusID",
                table: "BUSHASSTOP",
                column: "BusID");

            migrationBuilder.CreateIndex(
                name: "IX_BUSHASSTOP_StopID",
                table: "BUSHASSTOP",
                column: "StopID");

            migrationBuilder.CreateIndex(
                name: "IX_CREDITCARD_PassengerID",
                table: "CREDITCARD",
                column: "PassengerID");

            migrationBuilder.CreateIndex(
                name: "IX_DRIVER_UserAccountId",
                table: "DRIVER",
                column: "UserAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_JOURNEY_BusID",
                table: "JOURNEY",
                column: "BusID");

            migrationBuilder.CreateIndex(
                name: "IX_JOURNEY_EndBusStopID",
                table: "JOURNEY",
                column: "EndBusStopID");

            migrationBuilder.CreateIndex(
                name: "IX_JOURNEY_StartBusStopID",
                table: "JOURNEY",
                column: "StartBusStopID");

            migrationBuilder.CreateIndex(
                name: "IX_PASSENGER_UserAccountId",
                table: "PASSENGER",
                column: "UserAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_ROLEHASMENU_MenuID",
                table: "ROLEHASMENU",
                column: "MenuID");

            migrationBuilder.CreateIndex(
                name: "IX_ROLEHASMENU_RoleID",
                table: "ROLEHASMENU",
                column: "RoleID");

            migrationBuilder.CreateIndex(
                name: "IX_TICKET_PassengerID",
                table: "TICKET",
                column: "PassengerID");

            migrationBuilder.CreateIndex(
                name: "IX_TICKETHASJOURNEY_JourneyID",
                table: "TICKETHASJOURNEY",
                column: "JourneyID");

            migrationBuilder.CreateIndex(
                name: "IX_TICKETHASJOURNEY_TicketID",
                table: "TICKETHASJOURNEY",
                column: "TicketID");

            migrationBuilder.CreateIndex(
                name: "IX_USER_TOKEN_UserAccountId",
                table: "USER_TOKEN",
                column: "UserAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_USERACCOUNT_RoleId",
                table: "USERACCOUNT",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ADMIN");

            migrationBuilder.DropTable(
                name: "BUSHASDRIVER");

            migrationBuilder.DropTable(
                name: "BUSHASSTOP");

            migrationBuilder.DropTable(
                name: "CREDITCARD");

            migrationBuilder.DropTable(
                name: "PasswordProfileDto");

            migrationBuilder.DropTable(
                name: "ROLEHASMENU");

            migrationBuilder.DropTable(
                name: "TICKETHASJOURNEY");

            migrationBuilder.DropTable(
                name: "USER_TOKEN");

            migrationBuilder.DropTable(
                name: "DRIVER");

            migrationBuilder.DropTable(
                name: "MENU");

            migrationBuilder.DropTable(
                name: "JOURNEY");

            migrationBuilder.DropTable(
                name: "TICKET");

            migrationBuilder.DropTable(
                name: "BUS");

            migrationBuilder.DropTable(
                name: "BUSSTOP");

            migrationBuilder.DropTable(
                name: "PASSENGER");

            migrationBuilder.DropTable(
                name: "USERACCOUNT");

            migrationBuilder.DropTable(
                name: "ROLE");
        }
    }
}
