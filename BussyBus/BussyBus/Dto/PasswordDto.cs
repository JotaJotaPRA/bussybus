﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class PasswordDto
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Token is required")]
        public string Token { get; set; }

        [StringLength(100, ErrorMessage = "The email cannot overcome 100 characters.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(30, ErrorMessage = "Password have to be between 5 and 30.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(30, ErrorMessage = "ConfirmPassword have to be between 5 and 30.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
