﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class JourneyFilterDto
    {
        public int ID { get; set; }
        public int PriceMin { get; set; }
        public int PriceMax { get; set; }

        public DateTime Date { get; set; }
        public int CodeStart { get; set; }
        public int CodeEnd { get; set; }

        public string Ordering { get; set; }
    }
}
