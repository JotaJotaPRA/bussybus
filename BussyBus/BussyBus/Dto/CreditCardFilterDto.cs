﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class CreditCardFilterDto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string CreditCardNumber { get; set; }

        public string CVV { get; set; }

        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }

        public string Ordering { get; set; }
    }
}
