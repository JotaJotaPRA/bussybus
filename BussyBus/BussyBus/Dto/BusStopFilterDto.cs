﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class BusStopFilterDto
    {
        public int ID { get; set; }

        public int NumberMin { get; set; }
        public int NumberMax { get; set; }
        public string Direction { get; set; }
        public string Ordering { get; set; }
    }
}
