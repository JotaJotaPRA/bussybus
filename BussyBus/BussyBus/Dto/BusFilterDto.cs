﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class BusFilterDto
    {
        public int ID { get; set; }

        public int CapacityMin { get; set; }
        public int CapacityMax { get; set; }

        public int CodeMin { get; set; }
        public int CodeMax { get; set; }

        public string Ordering { get; set; }
    }
}
