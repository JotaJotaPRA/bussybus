﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class BusHasStopFilterDto
    {
        public int ID { get; set; }

        public int BusCodeMin { get; set; }
        public int BusCodeMax { get; set; }
        public int StopNumberMin { get; set; }
        public int StopNumberMax { get; set; }
        public string Ordering { get; set; }
    }
}
