﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class DriverFilterDto
    {
        public int ID { get; set; }
        public int CodeMin { get; set; }
        public int CodeMax { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Phone { get; set; }

        public string Username { get; set; }
        public string NSS { get; set; }
        public string DNI { get; set; }
        public string Direction { get; set; }
        public int Code { get; set; }
        public string AdditionalInfo { get; set; }

        public string Ordering { get; set; }
    }
}
