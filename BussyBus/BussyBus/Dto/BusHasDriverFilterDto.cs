﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class BusHasDriverFilterDto
    {
        public int ID { get; set; }

        public int DriverCodeMin { get; set; }
        public int DriverCodeMax { get; set; }
        public int BusCodeMin { get; set; }
        public int BusCodeMax { get; set; }
        public string Ordering { get; set; }
    }
}
