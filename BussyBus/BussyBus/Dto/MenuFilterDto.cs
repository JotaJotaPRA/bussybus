﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class MenuFilterDto
    {
        public int ID { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Label { get; set; }
        public string Ordering { get; set; }
    }
}
