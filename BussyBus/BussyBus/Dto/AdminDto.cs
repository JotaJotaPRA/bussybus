﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Dto
{
    public class AdminDto
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
        [Required(ErrorMessage = "username is required")]
        [StringLength(30, ErrorMessage = "Name have to be between 5 and 10.", MinimumLength = 5)]
        [Index(IsUnique = true)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(20, ErrorMessage = "Password have to be between 5 and 10.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [StringLength(20, ErrorMessage = "Password have to be between 5 and 10.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [StringLength(100, ErrorMessage = "The email cannot overcome 100 characters.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The name cannot overcome 20 characters.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(75, ErrorMessage = "The surname cannot overcome 75 characters.")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "The phone cannot overcome 13 characters")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone")]
        public string Phone { get; set; }
    }
}
