﻿using BussyBus.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Filter
{
    public class Security : IActionFilter
    {
        private readonly BussyBusDBContext _context;

        public Security(BussyBusDBContext context)
        {
            _context = context;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //se ha ejecutado
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            //se esta ejecutando
            var userId = Convert.ToInt32(context.HttpContext.Session.GetString("user"));
            //me quedo con el primero
            var user = _context.UserAccounts.Where(u => u.ID == userId).FirstOrDefault();

            if (user == null)
            {       // es lo mismo los dos el de abajo valdria porque en login solo esta login en otros con mas no valdria
                context.Result = new RedirectToActionResult("Login", "Login", null);
                //new RedirectResult("Login");
            }
        }
    }
}
