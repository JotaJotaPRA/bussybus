﻿using BussyBus.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Filter
{
    public class SecurityAdminPassenger : IActionFilter
    {
        private readonly BussyBusDBContext _context;

        public SecurityAdminPassenger(BussyBusDBContext context)
        {
            _context = context;
        }


        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var userId = Convert.ToInt32(context.HttpContext.Session.GetString("user"));

            var user = _context.UserAccounts
                .Where(u => u.ID == userId)
                .Include(u => u.Role)
                .FirstOrDefault();

            var role = _context.Roles.Where(r => r.ID == user.RoleId).FirstOrDefault();

            if (role == null)
            {
                context.Result = new RedirectToActionResult("Index", "Login", null);
            }
            else if (!role.Name.ToUpper().Equals("PASSENGER") &&
                !role.Name.ToUpper().Equals("ADMIN"))
            {
                context.Result = new RedirectToActionResult("Index", "Home", null);
            }

        }
    }
}

