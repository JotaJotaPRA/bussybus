﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Bus
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Capacity")]
        [Range(1, 100)]
        public int Capacity { get; set; }
        
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
        
        //Relaciones
        [InverseProperty("Bus")]
        public List<BusHasDriver> ListBusHasDrivers { get; set; }
        
        [InverseProperty("Bus")]
        public List<BusHasStop> ListBusHasStops { get; set; }
    }
}
