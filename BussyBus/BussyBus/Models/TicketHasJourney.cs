﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class TicketHasJourney
    {
        public int ID { get; set; }
        [ForeignKey("JourneyID")]
        [Display(Name = "Journey")]
        public Journey Journey { get; set; }

        [Display(Name = "Journey")]
        public int JourneyID { get; set; }

        [ForeignKey("TicketID")]
        [Display(Name = "Ticket")]
        public Ticket Ticket { get; set; }

        [Display(Name = "Ticket")]
        public int TicketID { get; set; }

    }
}
