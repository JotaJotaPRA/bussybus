﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public abstract class Person
    {
        public int ID { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "The name cannot overcome 20 characters.")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(75, ErrorMessage = "The surname cannot overcome 75 characters.")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "The phone cannot overcome 13 characters")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone")]
        public string Phone { get; set; }

        //Relaciones
        [ForeignKey("UserAccountId")]
        [Display(Name = "User Account")]
        public UserAccount UserAccount { get; set; }

        [Display(Name = "UserAccount")]
        public int UserAccountId { get; set; }
    }
}
