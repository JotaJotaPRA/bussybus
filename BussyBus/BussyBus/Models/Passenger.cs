﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Passenger : Person
    {
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
        //Relaciones
        [InverseProperty("Passenger")]
        public List<Ticket> ListTickets { get; set; }
        
        [InverseProperty("Passenger")]
        public List<CreditCard> ListCreditCards { get; set; }
    }
}
