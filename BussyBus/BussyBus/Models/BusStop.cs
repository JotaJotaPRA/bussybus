﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class BusStop
    {
        public int ID { get; set; }
        
        [Required]
        [Display(Name = "Number")]
        public int Number { get; set; }
        
        [Required]
        [StringLength(60, ErrorMessage = "The direction cannot overcome 20 characters.")]
        [Display(Name = "Direction")]
        public string Direction { get; set; }
        
        //Relaciones
        [InverseProperty("BusStop")]
        public List<BusHasStop> ListBusHasStops { get; set; }
        
        [InverseProperty("StartBusStop")]
        public List<Journey> ListStartJourneys { get; set; }
        
        [InverseProperty("EndBusStop")]
        public List<Journey> ListEndJourneys { get; set; }
        
    }
}
