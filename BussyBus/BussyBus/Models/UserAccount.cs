﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class UserAccount
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "username is required")]
        [StringLength(30, ErrorMessage = "Username have to be between 5 and 10.", MinimumLength = 5)]
        [Index(IsUnique = true)]
        [Display(Name = "User Name")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(100, ErrorMessage = "The email cannot overcome 100 characters.")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [Index(IsUnique = true)]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Active")]
        public bool Active { get; set; }

        //Relaciones
        [ForeignKey("RoleId")]
        [Display(Name = "Role")]
        public Role Role { get; set; }

        [Display(Name = "Role")]
        public int RoleId { get; set; }

        [InverseProperty("UserAccount")]
        public List<UserToken> ListUserToken { get; set; }
    }
}
