﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Role
    {
        public int ID { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(10, ErrorMessage = "Must be between 5 and 10 characters", MinimumLength = 5)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [StringLength(100, ErrorMessage = "Must be between 0 and 100 characters", MinimumLength = 0)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        //Relaciones
        [InverseProperty("Role")]
        public List<UserAccount> ListUserAccounts { get; set; }

        [InverseProperty("Role")]
        public List<RoleHasMenu> ListRoleHasMenus { get; set; }
    }
}
