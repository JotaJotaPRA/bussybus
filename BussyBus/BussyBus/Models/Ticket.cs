﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Ticket
    {
        public int ID { get; set; }
        
        [DataType(DataType.DateTime)]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }

        public string  StartBusStop { get; set; }
        public string EndBusStop { get; set; }
        [Required]
        [Display(Name = "Price")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Total { get; set; }


        //Relaciones
        [ForeignKey("PassengerID")]
        [Display(Name = "Passenger")]
        public Passenger Passenger { get; set; }
        
        [Display(Name = "Passenger")]
        public int PassengerID { get; set; }
        
        [InverseProperty("Ticket")]
        public List<TicketHasJourney> ListTicketHasJourneys { get; set; }
    }
}
