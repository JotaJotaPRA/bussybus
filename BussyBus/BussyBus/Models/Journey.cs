﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Journey
    {
        public int ID { get; set; }
        
        [Required]
        [Display(Name = "Price")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Journey Date")]
        public DateTime JourneyDate { get; set; }
        
        //Relaciones
        
        [ForeignKey("StartBusStopID")]
        [Display(Name = "StartBusStop")]
        public virtual BusStop StartBusStop { get; set; }
        
        [Display(Name = "StartBusStop")]
        public int StartBusStopID { get; set; }
        
        [ForeignKey("EndBusStopID")]
        [Display(Name = "EndBusStop")]
        public BusStop EndBusStop { get; set; }
        
        [Display(Name = "EndBusStop")]
        public int? EndBusStopID { get; set; }
        
        [ForeignKey("BusID")]
        [Display(Name = "Bus")]
        public Bus Bus { get; set; }

        [Display(Name = "Bus")]
        public int BusID { get; set; }
        [InverseProperty("Journey")]
        public List<TicketHasJourney> ListTicketHasJourneys { get; set; }
    }
}
