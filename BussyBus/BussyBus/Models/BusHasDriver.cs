﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class BusHasDriver
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }

        [ForeignKey("BusID")]
        [Display(Name = "Bus")]
        public Bus Bus { get; set; }
        
        [Display(Name = "Bus")]
        public int BusID { get; set; }
        
        //Relaciones
        [ForeignKey("DriverID")]
        [Display(Name = "Bus")]
        public Driver Driver { get; set; }
        
        [Display(Name = "Driver")]
        public int DriverID { get; set; }
    }
}
