﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Admin : Person
    {
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
    }
}
