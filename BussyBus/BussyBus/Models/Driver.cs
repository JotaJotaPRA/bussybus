﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Driver : Person
    {
        [Required]
        [Display(Name = "NSS")]
        [RegularExpression(@"^[0-9]{11}$", ErrorMessage = "NSS not valid.")]
        public string NSS { get; set; }
        
        [Required]
        [Display(Name = "DNI")]
        [RegularExpression(@"^[0-8]{8}$", ErrorMessage = "DNI not valid.")]
        public string DNI { get; set; }
        
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
        
        [Required]
        [StringLength(30, ErrorMessage = "The direction cannot overcome 20 characters.")]
        [Display(Name = "Direction")]
        public string Direction { get; set; }
        
        [StringLength(20, ErrorMessage = "The additional info cannot overcome 20 characters.")]
        [Display(Name = "Additional Info")]
        public string AdditionalInfo { get; set; }
        
        //Relaciones
        [InverseProperty("Driver")]
        public List<BusHasDriver> ListBusHasDrivers { get; set; }

    }
}
