﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class UserToken
    {
        public int ID { get; set; }

        [Required]
        [Index(IsUnique = true)]
        public string Token { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Generation Date")]
        public DateTime GenerationDate { get; set; }


        [Required]
        [Range(0, 20000, ErrorMessage = "Life must be between 0 and 20000 minutes")]
        public int Life { get; set; }

        //Relaciones
        [ForeignKey("UserAccountId")]
        [Display(Name = "User Account")]
        public UserAccount UserAccount { get; set; }

        [Display(Name = "UserAccount")]
        public int UserAccountId { get; set; }
    }
}
