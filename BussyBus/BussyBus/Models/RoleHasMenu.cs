﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class RoleHasMenu
    {
        public int ID { get; set; }

        //Relaciones
        [ForeignKey("RoleID")]
        [Display(Name = "Role")]
        public Role Role { get; set; }
        
        [Display(Name = "Role")]
        public int RoleID { get; set; }
        
        [ForeignKey("MenuID")]
        [Display(Name = "Menu")]
        public Menu Menu { get; set; }
        
        [Display(Name = "Menu")]
        public int MenuID { get; set; }
    }
}
