﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class Menu
    {
        public int ID { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "Controller must have al least 1 character")]
        [MaxLength(20, ErrorMessage = "Controller must have less than 20 characters")]
        public string Controller { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "Action must have al least 1 character")]
        [MaxLength(20, ErrorMessage = "Action must have less than 20 characters")]
        public string Action { get; set; }

        [Required]
        [MinLength(1, ErrorMessage = "Label must have al least 1 character")]
        [MaxLength(20, ErrorMessage = "Label must have less than 20 characters")]
        public string Label { get; set; }

        //Relaciones
        [InverseProperty("Menu")]
        public List<RoleHasMenu> ListRoleHasMenus { get; set; }

        [NotMapped]
        public string Fullname { get => (Controller + " - " + Action); }
    }
}
