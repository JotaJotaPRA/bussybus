﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class CreditCard
    {
        public int ID { get; set; }
        
        [Required]
        [StringLength(20, ErrorMessage = "The name cannot overcome 20 characters.")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        
        [Required]
        [Display(Name = "Credit Card Number")]
        [CreditCard]
        public string CreditCardNumber { get; set; }
        
        [Required]
        [Display(Name = "CVV")]
        [StringLength(3, ErrorMessage = "The cvv cannot overcome 3 characters")]
        public string CVV { get; set; }
        
        [Required]
        [Range(1, 12)]
        [Display(Name = "Expiration Month")]
        public int ExpirationMonth { get; set; }
        
        [Required]
        [Range(2021, 2100)]
        [Display(Name = "Expiration Year")]
        public int ExpirationYear { get; set; }
        
        //Relaciones
        [ForeignKey("PassengerID")]
        [Display(Name = "Passenger")]
        public Passenger Passenger { get; set; }

        [Display(Name = "Passenger")]
        public int PassengerID { get; set; }
    }
}
