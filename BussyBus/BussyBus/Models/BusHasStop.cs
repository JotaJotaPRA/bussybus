﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BussyBus.Models
{
    public class BusHasStop
    {
        public int ID { get; set; }
        [Required]
        [Display(Name = "Code")]
        public int Code { get; set; }
        //Relaciones
        [ForeignKey("BusID")]
        [Display(Name = "Bus")]
        public Bus Bus { get; set; }
        
        [Display(Name = "Bus")]
        public int BusID { get; set; }
        
        [ForeignKey("StopID")]
        [Display(Name = "Bus")]
        public BusStop BusStop { get; set; }
        
        [Display(Name = "Stop")]
        public int StopID { get; set; }
    }
}
