﻿using BussyBus.Models;
using BussyBus.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BussyBus.Dto;

namespace BussyBus.Data
{
    public class BussyBusDBContext : DbContext
    {
        public BussyBusDBContext()
        {

        }
        public BussyBusDBContext(DbContextOptions<BussyBusDBContext> options)
            : base(options)
        {

        }


        public DbSet<Admin> Admins { get; set; }
        public DbSet<Bus> Buses { get; set; }
        public DbSet<BusHasDriver> BusHasDrivers { get; set; }
        public DbSet<BusHasStop> BusHasStops { get; set; }
        public DbSet<BusStop> BusStops { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<Journey> Journeys { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<Passenger> Passengers { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleHasMenu> RoleHasMenus { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketHasJourney> TicketHasJourneys { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfiguration configuration = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>().ToTable("ADMIN");
            modelBuilder.Entity<Bus>().ToTable("BUS");
            modelBuilder.Entity<BusHasDriver>().ToTable("BUSHASDRIVER");
            modelBuilder.Entity<BusHasStop>().ToTable("BUSHASSTOP");
            modelBuilder.Entity<BusStop>().ToTable("BUSSTOP");
            modelBuilder.Entity<CreditCard>().ToTable("CREDITCARD");
            modelBuilder.Entity<Driver>().ToTable("DRIVER");
            modelBuilder.Entity<Journey>().ToTable("JOURNEY");
            modelBuilder.Entity<Menu>().ToTable("MENU");
            modelBuilder.Entity<Passenger>().ToTable("PASSENGER");
            modelBuilder.Entity<Role>().ToTable("ROLE");
            modelBuilder.Entity<RoleHasMenu>().ToTable("ROLEHASMENU");
            modelBuilder.Entity<Ticket>().ToTable("TICKET");
            modelBuilder.Entity<TicketHasJourney>().ToTable("TICKETHASJOURNEY");
            modelBuilder.Entity<UserAccount>().ToTable("USERACCOUNT");
            modelBuilder.Entity<UserToken>().ToTable("USER_TOKEN");

            modelBuilder.Entity<Passenger>()
                .HasOne(c => c.UserAccount)
                .WithMany()
                .HasForeignKey("UserAccountId")
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Driver>()
                .HasOne(t => t.UserAccount)
                .WithMany()
                .HasForeignKey("UserAccountId")
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Admin>()
                .HasOne(a => a.UserAccount)
                .WithMany()
                .HasForeignKey("UserAccountId")
                .OnDelete(DeleteBehavior.NoAction);


            modelBuilder.Entity<Role>().HasData(
                new Role
                {
                    ID = 1,
                    Name = "ADMIN"
                },
                new Role
                {
                    ID = 2,
                    Name = "DRIVER"
                },
                new Role
                {
                    ID = 3,
                    Name = "PASSENGER"
                }
            );

            modelBuilder.Entity<UserAccount>().HasData(
               new UserAccount
               {
                   ID = 1,
                   Username = "admin1",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = true,
                   Email = "admin1@gmail.com",
                   RoleId = 1

               },
               new UserAccount
               {
                   ID = 2,
                   Username = "admin2",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = false,
                   Email = "admin2@gmail.com",
                   RoleId = 1

               },
               new UserAccount
               {
                   ID = 3,
                   Username = "driver1",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = true,
                   Email = "driver1@gmail.com",
                   RoleId = 2

               },
               new UserAccount
               {
                   ID = 4,
                   Username = "driver2",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = false,
                   Email = "driver2@gmail.com",
                   RoleId = 2

               },
               new UserAccount
               {
                   ID = 5,
                   Username = "passenger1",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = true,
                   Email = "passenger1@gmail.com",
                   RoleId = 3

               },
               new UserAccount
               {
                   ID = 6,
                   Username = "passenger2",
                   Password = Utility.Encrypt("Altair123$%"),
                   Active = false,
                   Email = "passenger2@gmail.com",
                   RoleId = 3

               }
           );

            modelBuilder.Entity<Admin>().HasData(
               new Admin
               {
                   ID = 1,
                   Code = 101,
                   Name = "admin1",
                   Surname = "admin1",
                   Phone = "611111111",
                   UserAccountId = 1
               },
               new Admin
               {
                   ID = 2,
                   Code = 102,
                   Name = "admin2",
                   Surname = "admin2",
                   Phone = "622222222",
                   UserAccountId = 2
               }
           ); ;

            modelBuilder.Entity<Driver>().HasData(
               new Driver
               {
                   ID = 1,
                   Code = 101,
                   Name = "driver1",
                   Surname = "driver1",
                   Phone = "655555555",
                   NSS = "12345678901",
                   DNI = "12345678A",
                   Direction ="Calle Balonmano",
                   UserAccountId = 3
               },
               new Driver
               {
                   ID = 2,
                   Code = 102,
                   Name = "driver2",
                   Surname = "driver2",
                   Phone = "666666667",
                   NSS = "12345678902",
                   DNI = "12345679B",
                   Direction = "Calle Balonmano",
                   UserAccountId = 4
               }
           );
            modelBuilder.Entity<Passenger>().HasData(
               new Passenger
               {
                   ID = 1,
                   Code = 101,
                   Name = "passenger1",
                   Surname = "passenger1",
                   Phone = "633333333",
                   UserAccountId = 5
               },
               new Passenger
               {
                   ID = 2,
                   Code = 102,
                   Name = "passenger2",
                   Surname = "passenger2",
                   Phone = "644444444",
                   UserAccountId = 6
               }
           );


            modelBuilder.Entity<Menu>().HasData(
                new Menu
                {
                    ID = 1,
                    Controller = "Passengers",
                    Action = "Index",
                    Label = "Passengers"
                },
                new Menu
                {
                    ID = 2,
                    Controller = "Drivers",
                    Action = "Index",
                    Label = "Drivers"
                },
                new Menu
                {
                    ID = 3,
                    Controller = "Admins",
                    Action = "Index",
                    Label = "Admins"
                },
                new Menu
                {
                    ID = 4,
                    Controller = "Buses",
                    Action = "Index",
                    Label = "Bus"
                },
                new Menu
                {
                    ID = 5,
                    Controller = "BusHasDrivers",
                    Action = "Index",
                    Label = "BusHasDriver"
                },
                new Menu
                {
                    ID = 6,
                    Controller = "BusHasStops",
                    Action = "Index",
                    Label = "BusHasStop"
                },
                new Menu
                {
                    ID = 7,
                    Controller = "BusStops",
                    Action = "Index",
                    Label = "BusStop"
                },
                new Menu
                {
                    ID = 8,
                    Controller = "CreditCards",
                    Action = "Index",
                    Label = "CreditCard"
                },
                new Menu
                {
                    ID = 9,
                    Controller = "Journeys",
                    Action = "Index",
                    Label = "Journey"
                },
                new Menu
                {
                    ID = 10,
                    Controller = "Menus",
                    Action = "Index",
                    Label = "Menu"
                },
                new Menu
                {
                    ID = 11,
                    Controller = "Roles",
                    Action = "Index",
                    Label = "Role"
                },
                new Menu
                {
                    ID = 12,
                    Controller = "RoleHasMenus",
                    Action = "Index",
                    Label = "RoleHasMenu"
                },
                new Menu
                {
                    ID = 13,
                    Controller = "Tickets",
                    Action = "Index",
                    Label = "Ticket"
                },
                new Menu
                {
                    ID = 14,
                    Controller = "UserAccounts",
                    Action = "Index",
                    Label = "UserAccount"
                },
                new Menu
                {
                    ID = 15,
                    Controller = "Admins",
                    Action = "Details",
                    Label = "Profile"
                },
                new Menu
                {
                    ID = 16,
                    Controller = "Drivers",
                    Action = "Details",
                    Label = "Profile"
                },
                new Menu
                {
                    ID = 17,
                    Controller = "Passengers",
                    Action = "Details",
                    Label = "Profile"
                },
                new Menu
                {
                    ID = 18,
                    Controller = "TicketHasJourneys",
                    Action = "Index",
                    Label = "TicketHasJourneys"
                }
            );

            modelBuilder.Entity<RoleHasMenu>().HasData(
                new RoleHasMenu
                {
                    ID = 1,
                    RoleID = 1,
                    MenuID = 2
                },
                new RoleHasMenu
                {
                    ID = 2,
                    RoleID = 1,
                    MenuID = 3
                },
                new RoleHasMenu
                {
                    ID = 3,
                    RoleID = 1,
                    MenuID = 1
                },
                new RoleHasMenu
                {
                    ID = 4,
                    RoleID = 1,
                    MenuID = 4
                },
                new RoleHasMenu
                {
                    ID = 5,
                    RoleID = 1,
                    MenuID = 6
                },
                new RoleHasMenu
                {
                    ID = 6,
                    RoleID = 1,
                    MenuID = 7
                },
                new RoleHasMenu
                {
                    ID = 7,
                    RoleID = 1,
                    MenuID = 12
                },
                new RoleHasMenu
                {
                    ID = 8,
                    RoleID = 1,
                    MenuID = 13
                },
                new RoleHasMenu
                {
                    ID = 9,
                    RoleID = 1,
                    MenuID = 9
                },
                new RoleHasMenu
                {
                    ID = 10,
                    RoleID = 1,
                    MenuID = 14
                },
                new RoleHasMenu
                {
                    ID = 11,
                    RoleID = 1,
                    MenuID = 11
                },
                new RoleHasMenu
                {
                    ID = 12,
                    RoleID = 1,
                    MenuID = 10
                },
                new RoleHasMenu
                {
                    ID = 13,
                    RoleID = 1,
                    MenuID = 15
                },
                new RoleHasMenu
                {
                    ID = 14,
                    RoleID = 1,
                    MenuID = 5
                },
                new RoleHasMenu
                {
                    ID = 15,
                    RoleID = 1,
                    MenuID = 18
                },
                new RoleHasMenu
                {
                    ID = 16,
                    RoleID = 2,
                    MenuID = 5
                },
                new RoleHasMenu
                {
                    ID = 18,
                    RoleID = 2,
                    MenuID = 9
                },
                new RoleHasMenu
                {
                    ID = 19,
                    RoleID = 2,
                    MenuID = 16
                },
                new RoleHasMenu
                {
                    ID = 21,
                    RoleID = 3,
                    MenuID = 8
                },
                new RoleHasMenu
                {
                    ID = 22,
                    RoleID = 3,
                    MenuID = 13
                },
                new RoleHasMenu
                {
                    ID = 23,
                    RoleID = 3,
                    MenuID = 9
                },
                new RoleHasMenu
                {
                    ID = 24,
                    RoleID = 3,
                    MenuID = 17
                }/*,
                new RoleHasMenu
                {
                    ID = 24,
                    RoleID = 1,
                    MenuID = 15
                },
                new RoleHasMenu
                {
                    ID = 25,
                    RoleID = 4,
                    MenuID = 6
                },*/

            );
        }

        public DbSet<BussyBus.Dto.PasswordProfileDto> PasswordProfileDto { get; set; }

        public DbSet<BussyBus.Models.TicketHasJourney> TicketHasJourney { get; set; }
    }
}
